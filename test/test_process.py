from cProfile import run
from concurrent.futures import Future
import inspect
from multiprocessing import context
import os
from pathlib import Path
import platform
import unittest
from unittest import mock
from hoppr.configs.credentials import Credentials
from hoppr.configs.manifest import Manifest
from hoppr.context import Context
from hoppr.base_plugins.hoppr import hoppr_process
from hoppr.base_plugins.hoppr import HopprPlugin
from hoppr.configs.transfer import Transfer
from hoppr import utils
from hoppr.mem_logger import MemoryLogger
from hoppr.hoppr_types.cred_object import CredObject
from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from hoppr.hoppr_types.transfer_file_content import Plugin
from hoppr.processor import StageProcessor
from hoppr.processor import HopprProcessor
from hoppr.result import Result
from hoppr_cyclonedx_models.cyclonedx_1_4 import CyclonedxSoftwareBillOfMaterialsStandard as Bom

import hoppr.main


class UnitTestPlugin(HopprPlugin):

    def get_version(self) -> str:
        return "0.1.2"

    @hoppr_process
    def pre_stage_process(self):
        print(f"Hiya World")
        self.get_logger().info(f"Hiya World")
        return Result.success()

    @hoppr_process
    def post_stage_process(self):
        print(f"Ta-ta World")
        self.get_logger().info(f"Ta-ta World")
        return Result.retry("try again")


class TestHopprProcess(unittest.TestCase):

    @mock.patch.object(HopprProcessor, '_collect_file')
    @mock.patch.object(StageProcessor, 'run', return_value=Result.success())
    @mock.patch.object(Manifest, 'load_file')
    def test_run_success(self, mock_manifest_load, mock_stage_run, mock_collect_file):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        proc = HopprProcessor(transfer, manifest)
        proc.metadata_files = ["dummy_metadata"]

        result = proc.run()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

    @mock.patch.object(os, "getlogin", return_value="Fake User")
    @mock.patch.object(platform, "system", return_value="Windows")
    @mock.patch.object(HopprProcessor, '_collect_file')
    @mock.patch.object(StageProcessor, 'run', return_value=Result.success())
    @mock.patch.object(Manifest, 'load_file')
    def test_run_success_platform_windows(self, mock_manifest_load, mock_stage_run, mock_collect_file, mock_platform, mock_oslogin):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        proc = HopprProcessor(transfer, manifest)
        proc.metadata_files = ["dummy_metadata"]

        result = proc.run()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

    @mock.patch.object(HopprProcessor, '_summarize_results', return_value=42)
    @mock.patch.object(StageProcessor, 'run', return_value=Result.fail("oops"))
    @mock.patch.object(Manifest, 'load_file')
    def test_run_fail(self, mock_manifest_load, mock_stage_run, mock_summarize_results):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        proc = HopprProcessor(transfer, manifest)
        result = proc.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"

        assert proc.get_logger() is not None

    @mock.patch.object(Manifest, 'load_file')
    def test_summary_success(self, mock_manifest_load, ):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
                manifest=manifest,
                collect_root_dir="ROOT",
                consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
                max_processes=3
            )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor("test-stage", context, None)
        stage.results["pre_stage_process"] = {
            ("plugin-a", None, Result.success()),
            ("plugin-a", None, Result.fail()),
            ("plugin-a", None, Result.retry()),
        }
        stage.results["process_component"] = {
            ("plugin-a", "my-component", Result.success()),
            ("plugin-a", "my-component", Result.fail()),
            ("plugin-a", "my-component", Result.retry()),
        }
        proc.stages["test-stage"] = stage

        failure_count = proc._summarize_results()

        assert failure_count == 4

test_component_list = [
        Component(
            name="transfer-test",
            type="file",
            purl="pkg:generic/README.md",
        ),
        Component(
            name="manifest",
            type="file",
            purl="pkg:generic/docs/CHANGELOG.md",
        ),
    ]

class TestStageProcess(unittest.TestCase):
    def test_run_plugin(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        context = Context(
                manifest=None,
                collect_root_dir="ROOT",
                consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
                max_processes=3
        )

        plugin = UnitTestPlugin(context)

        result = hoppr.processor._run_plugin(plugin, "pre_stage_process", None)
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        result = hoppr.processor._run_plugin(plugin, "process_component", None)
        assert result.is_skip(), f"Expected SKIP result, got {result}"

        result = hoppr.processor._run_plugin(plugin, "post_stage_process", None)
        assert result.is_retry(), f"Expected RETRY result, got {result}"

        result = hoppr.processor._run_plugin(plugin, "not_a_real_process", None)
        assert result.is_fail(), f"Expected FAIL result, got {result}"

    @mock.patch('hoppr.processor.plugin_instance', return_value = UnitTestPlugin(Context(Manifest(), "ROOT", None, 3)))
    @mock.patch.object(Manifest, 'load_file')
    def test_stage_run_success(self, mock_manifest_load, mock_plugin_instance):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
                manifest=manifest,
                collect_root_dir="ROOT",
                consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
                max_processes=3
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor("test-stage", context, None)
        stage.context.consolidated_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        result = stage.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "1 'post_stage_process' processes returned 'retry'"

    @mock.patch('hoppr.processor.plugin_instance', side_effect = ModuleNotFoundError())
    @mock.patch.object(Manifest, 'load_file')
    def test_stage_run_module_not_found(self, mock_manifest_load, mock_plugin_instance):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
                manifest=manifest,
                collect_root_dir="ROOT",
                consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
                max_processes=3
            )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor("test-stage", context, None)
        stage.context.consolidated_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        result = stage.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"

    @mock.patch.object(Future, 'result', return_value = Result.fail())
    @mock.patch('hoppr.processor.plugin_instance', return_value = UnitTestPlugin(None))
    @mock.patch.object(Manifest, 'load_file')
    def test_stage_run_all_fail(self, mock_manifest_load, mock_plugin_instance, mock_future_result):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
                manifest=manifest,
                collect_root_dir="ROOT",
                consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
                max_processes=3
            )

        proc = HopprProcessor(transfer, manifest)
        
        stage = StageProcessor("test-stage", context, None)
        stage.context.consolidated_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        result = stage.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "1 'pre_stage_process' processes failed\n2 'process_component' processes failed\n1 'post_stage_process' processes failed"

    @mock.patch.object(Future, 'result', side_effect = [
        Result.success(),
        Result.retry(), Result.fail(),
        Result.success(),
    ])
    @mock.patch('hoppr.processor.plugin_instance', return_value = UnitTestPlugin(None))
    @mock.patch.object(Manifest, 'load_file')
    def test_stage_run_fail_and_retry(self, mock_manifest_load, mock_plugin_instance, mock_future_result):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
                manifest=manifest,
                collect_root_dir="ROOT",
                consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
                max_processes=3
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor("test-stage", context, None)
        stage.context.consolidated_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        result = stage.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "1 'process_component' processes failed, and 1 returned 'retry'\n"

    @mock.patch("shutil.copyfile")
    @mock.patch.object(Manifest, 'load_file')
    def test_collect_file(self, mock_manifest_load, mock_copyfile):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
                manifest=manifest,
                collect_root_dir="ROOT",
                consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
                max_processes=3
            )

        proc = HopprProcessor(transfer, manifest)
        proc.context = context
        proc.logger = MemoryLogger("hoppr3.log", lock=context.logfile_lock, log_name="test_logger", flush_immed=True)

        proc._collect_file("/path/to/metadata_file_name", "target_dir/")

        mock_copyfile.assert_called_once_with("/path/to/metadata_file_name", "target_dir/%2Fpath%2Fto%2Fmetadata_file_name")
        proc.logger.close()
        os.remove("hoppr3.log")

    @mock.patch("hoppr.processor.download_file")
    @mock.patch.object(Credentials, "find_credentials", return_value=CredObject("un", "pw"))
    @mock.patch.object(Manifest, 'load_file')
    def test_collect_url(self, mock_manifest_load, mock_find_creds, mock_downloadfile):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
                manifest=manifest,
                collect_root_dir="ROOT",
                consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
                max_processes=3
            )

        proc = HopprProcessor(transfer, manifest)
        proc.context = context
        proc.logger = MemoryLogger("hoppr4.log", lock=context.logfile_lock, log_name="test_logger", flush_immed=True)

        proc._collect_url("http://metadata_url", "target_dir/")

        mock_downloadfile.assert_called_once_with("http://metadata_url", "target_dir/http%3A%2F%2Fmetadata_url", None)
        proc.logger.close()
        os.remove("hoppr4.log")

    @staticmethod
    @mock.patch.object(Manifest, "load_sbom", return_value=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"))
    def manifest_from_string(man_string, parent=None):
        manifest = Manifest()
        input_dict = utils.load_string(man_string)
        manifest = Manifest()
        manifest.populate(input_dict, parent)
        return manifest

    parent_manifest = """
schemaVersion: v1
kind: Manifest

metadata:
  name: "Test parent Manifest"
  version: 0.1.0
  description: "test parent manifest"

sboms: []

includes:
  - local: test/child-manifest.yml
  - url: http://dummy/empty_manifest.yml

repositories:
  generic:
    - url: https://media3.giphy.com/
"""

    child_manifest = """
schemaVersion: v1
kind: Manifest

metadata:
  name: "Test Child Manifest"
  version: 0.1.0
  description: "Test Child Manifest"

sboms:
  - local: test/bom.json
  - url: http://dummy/bom2.json

includes: []

repositories: {}
"""

    empty_manifest = """
schemaVersion: v1
kind: Manifest

metadata:
  name: "Test Empty Manifest"
  version: 0.1.0
  description: "Test Empty Manifest"

sboms: []

includes: []

repositories: {}
"""
    @mock.patch.object(Manifest, "load_file", return_value=manifest_from_string(child_manifest))
    @mock.patch.object(Manifest, "load_url", return_value=manifest_from_string(empty_manifest))
    @mock.patch.object(HopprProcessor, "_collect_file")
    @mock.patch.object(HopprProcessor, "_collect_url")
    def test_collect_manifest_metadata(self, mock_collect_url, mock_collect_file, mock_man_from_url, mock_man_from_file):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = self.manifest_from_string(self.parent_manifest)

        context = Context(
                manifest=manifest,
                collect_root_dir="ROOT",
                consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
                max_processes=3
            )

        proc = HopprProcessor(transfer, manifest)

        proc._collect_manifest_metadata(manifest, "target_dir/")
