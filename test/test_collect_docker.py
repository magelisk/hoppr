import inspect
import unittest
from unittest import mock

from hoppr.core_plugins.collect_docker_plugin import CollectDockerPlugin
from hoppr.configs.credentials import Credentials
from hoppr.result import Result
from hoppr.context import Context
from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from test.mock_objects import MockSubprocessRun
from hoppr.hoppr_types.cred_object import CredObject


class TestCollectorDocker(unittest.TestCase):

    def _create_test_plugin(self):
        context = Context(manifest="MANIFEST", collect_root_dir="COLLECTION_DIR", consolidated_sbom="BOM", retry_wait_seconds=1, max_processes=3)
        my_plugin = CollectDockerPlugin(context=context, config={"skopeo_command": "skopeo"})
        return my_plugin

    @mock.patch("subprocess.run", return_value=MockSubprocessRun(0))
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @mock.patch.object(CollectDockerPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collector_docker_success(self, mock_get_repos, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:docker/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"

    @mock.patch.object(Credentials, "find_credentials", return_value= CredObject("mock_user_name", "mock_password"))
    @mock.patch("os.remove",return_value=None)
    @mock.patch("os.path.exists", return_value=True)
    @mock.patch("subprocess.run", return_value=MockSubprocessRun(1))
    @mock.patch("os.mkdir", return_value = None)
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @mock.patch.object(CollectDockerPlugin, "_get_repos", return_value=["http://somewhere.com"])
    def test_collector_docker_fail(self, mock_get_repos, mock_cmd_check, mock_directory, mock_subprocess_run, mock_exists, mock_remove, mock_get_creds):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:docker/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message.startswith("Failure after 3 attempts, final message Skopeo failed to copy docker image")

    def test_get_version(self):
        my_plugin = self._create_test_plugin()
        assert len(my_plugin.get_version()) > 0

    @mock.patch("subprocess.run", return_value=MockSubprocessRun(0))
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.fail("[mock] command not found"))
    @mock.patch.object(CollectDockerPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collector_docker_command_not_found(self, mock_get_repos, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:docker/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message == "[mock] command not found"
