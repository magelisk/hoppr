import inspect
from pathlib import Path
import unittest
from hoppr.configs.transfer import Transfer

import pytest

class TestTransfer(unittest.TestCase):
    def test_success(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        transfer_config = Transfer.load_file(Path("./test/resources/transfer/transfer-test.yml"))

        assert transfer_config.content.kind == "Transfer"
        assert len(transfer_config.content.stages) == 2
        assert transfer_config.content.stages[0].name == "Collect"
        assert transfer_config.content.stages[1].name == "Bundle"
        assert transfer_config.content.stages[0].plugins[0].name == "hoppr.core_plugins.collect_docker_plugin"
        assert transfer_config.content.stages[1].plugins[0].config["tarfile_name"] == "~/tarfile.tar.gz"
        assert transfer_config.content.max_processes == 3
