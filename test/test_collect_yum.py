import inspect
import unittest
from unittest import mock

from hoppr.core_plugins.collect_yum_plugin import CollectYumPlugin
from hoppr.configs.credentials import Credentials
from hoppr.result import Result
from hoppr.context import Context
from hoppr.hoppr_types.cred_object import CredObject
from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from test.mock_objects import MockSubprocessRun


class TestCollectorYum(unittest.TestCase):

    def _create_test_plugin(self):
        context = Context(manifest="MANIFEST", collect_root_dir="COLLECTION_DIR", consolidated_sbom="BOM", retry_wait_seconds=1, max_processes=3)
        my_plugin = CollectYumPlugin(context=context, config={"yumdownloader_command": "yumdownloader"})
        return my_plugin

    @mock.patch("subprocess.run", return_value=MockSubprocessRun(0, stdout=b"https://somewhere.com"))
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @mock.patch.object(CollectYumPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collect_yum_success(self, mock_get_repos, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"

    @mock.patch.object(Credentials, "find_credentials", return_value=CredObject("mock_user_name", "mock_password"))
    @mock.patch("subprocess.run", return_value=MockSubprocessRun(1))
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @mock.patch.object(CollectYumPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collect_yum_fail(self, mock_get_repos, mock_cmd_check, mock_subprocess_run, mock_get_creds):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message.startswith("Failure after 3 attempts, final message yumdownloader failed to locate package for")

    def test_get_version(self):
        my_plugin = self._create_test_plugin()
        assert len(my_plugin.get_version()) > 0

    @mock.patch("subprocess.run", return_value=MockSubprocessRun(0))
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.fail("[mock] command not found"))
    @mock.patch.object(CollectYumPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collect_yum_command_not_found(self, mock_get_repos, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message == "[mock] command not found"

    @mock.patch("subprocess.run", return_value=MockSubprocessRun(0, stdout=b"https://elsewhere.com"))
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @mock.patch.object(CollectYumPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collect_yum_bad_repo(self, mock_get_repos, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected fail result, got {collect_result}"
        assert collect_result.message.startswith("Yum download url does not match requested url")

    @mock.patch("subprocess.run", side_effect=[
        MockSubprocessRun(0, stdout=b"https://somewhere.com"),
        MockSubprocessRun(1),
        MockSubprocessRun(0, stdout=b"https://somewhere.com"),
        MockSubprocessRun(1),
        MockSubprocessRun(0, stdout=b"https://somewhere.com"),
        MockSubprocessRun(1),
        ])
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @mock.patch.object(CollectYumPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collect_yum_fail_download(self, mock_get_repos, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to download Yum artifact")
