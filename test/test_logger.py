import inspect
import multiprocessing
import os
import re
import unittest
from hoppr.mem_logger import MemoryLogger


class TestLogger(unittest.TestCase):

    def test_logging(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        lock = multiprocessing.Manager().RLock()
        test_logger = MemoryLogger("hoppr1.log", lock)
        test_logger.debug("test debug message")
        test_logger.info("test info message")
        test_logger.warning("test warning message")
        test_logger.error("test error message")
        test_logger.fatal("test fatal message")
        test_logger.critical("test critical message")
        test_logger.flush()

        test_logger.close()

        with open('hoppr1.log', 'r') as file:
            captured = file.read()
        m = re.match(r".*INFO.*test info message\n.*WARNING.*test warning message\n.*ERROR.*test error message\n.*CRITICAL.*test fatal message\n.*CRITICAL.*test critical message", captured)
        assert m is not None

        os.remove("hoppr1.log")

    def test_clear_log(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        lock = multiprocessing.Manager().RLock()
        test_logger = MemoryLogger("hoppr2.log", lock)
        test_logger.info("test info message")
        test_logger.warning("test warning message")
        test_logger.error("test error message")
        test_logger.clear_targets()
        test_logger.close()

        with open('hoppr2.log', 'r') as file:
            captured = file.read()
        m = re.match(r"\s*", captured)
        assert m is not None
        os.remove("hoppr2.log")

    ##### Just for completeness . . . one should never run without a lock #####
    def test_logging_nolock(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        lock = None
        test_logger = MemoryLogger("hoppr1.log", lock)
        test_logger.debug("test debug message")
        test_logger.info("test info message")
        test_logger.warning("test warning message")
        test_logger.error("test error message")
        test_logger.fatal("test fatal message")
        test_logger.critical("test critical message")
        test_logger.flush()

        test_logger.close()

        with open('hoppr1.log', 'r') as file:
            captured = file.read()
        m = re.match(r".*INFO.*test info message\n.*WARNING.*test warning message\n.*ERROR.*test error message\n.*CRITICAL.*test fatal message\n.*CRITICAL.*test critical message", captured)
        assert m is not None

        os.remove("hoppr1.log")

    ##### Just for completeness . . . one should never run without a lock #####
    def test_clear_log_nolock(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        lock = None
        test_logger = MemoryLogger("hoppr2.log", lock)
        test_logger.info("test info message")
        test_logger.warning("test warning message")
        test_logger.error("test error message")
        test_logger.clear_targets()
        test_logger.close()

        with open('hoppr2.log', 'r') as file:
            captured = file.read()
        m = re.match(r"\s*", captured)
        assert m is not None
        os.remove("hoppr2.log")
