import json
import os
import pprint
import inspect
from pathlib import Path
import unittest
from unittest import mock

import yaml
from hoppr.configs.credentials import Credentials
from hoppr.configs.manifest import Manifest
from hoppr.exceptions import HopprError, HopprLoadDataError
from hoppr.hoppr_types.cred_object import CredObject
from hoppr.hoppr_types.manifest_file_content import Include, ManifestFileContent, Repository, SBOMRef
from hoppr.hoppr_types.purl_type import PurlType

import pytest

class TestManifest(unittest.TestCase):

    def test_manifest_includes(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        Manifest.loaded_manifests = []
        manifest = Manifest.load_file(Path("test", "resources", "manifest", "unit", "manifest.yml").resolve())
        base_includes = [ Include(local='test/resources/manifest/unit/manifest.yml'), Include(local='test/resources/manifest/unit/child-a.yml')]

        assert manifest.manifest_file_content.includes == base_includes
        assert len(manifest.children[0].manifest_file_content.includes) == 0

    def test_success(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        Manifest.loaded_manifests = []
        manifest = Manifest.load_file(Path("test", "resources", "manifest", "unit", "manifest.yml").resolve())

        assert manifest.manifest_file_content.kind == "Manifest"
        assert manifest.manifest_file_content.schema_version == "v1"
        assert manifest.manifest_file_content.metadata.name == "Parent - Unit Test Manifest"
        assert manifest.manifest_file_content.metadata.version == "0.1.0"

        assert len(manifest.children[0].sboms) == 1

    manifest_content = yaml.safe_load("""
---
schemaVersion: v1
kind: manifest

metadata:
    name: "Test Local Manifest"
    version: 0.1.0
    description: "Unit test manifest string"

sboms: []
includes: []
repositories: []
""")

    @mock.patch("hoppr.net.load_url", return_value=manifest_content)
    def test_load_by_url_no_credentials(self, mock_manifest):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        manifest = Manifest.load_url("http://127.0.0.1/fake-manifest.yml", None)
        assert manifest.manifest_file_content.metadata.name == "Test Local Manifest"

    @mock.patch("hoppr.net.load_url", return_value=manifest_content)
    @mock.patch.object(Credentials, "find_credentials", return_value=CredObject("username", "password"))
    def test_load_by_url_with_credentials(self, mock_credentials, mock_manifest):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        manifest = Manifest.load_url("http://127.0.0.1/fake-manifest.yml")
        assert manifest.manifest_file_content.metadata.name == "Test Local Manifest"

    @mock.patch("hoppr.net.load_url", return_value=manifest_content)
    def test_load_child_by_url(self, mock_child_manifest):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        Manifest.loaded_manifests = []
        manifest = Manifest.load_file(Path("test", "resources", "manifest", "unit", "manifest-url-include.yml").resolve())
        assert manifest.manifest_file_content.metadata.name == "Parent - Unit Test Manifest"
        assert len(manifest.children) == 1
        assert manifest.children[0].manifest_file_content.metadata.name == "Test Local Manifest"

    def test_load_bad_child(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        Manifest.loaded_manifests = []
        with pytest.raises(HopprLoadDataError):
            manifest = Manifest.load_file(Path("test", "resources", "manifest", "unit", "fails", "manifest-bad-includes.yml").resolve())

    def test_load_sbom_bad_location(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        sbom_location = SBOMRef()
        sbom_location.badlocation = ""
        with pytest.raises(HopprLoadDataError):
            Manifest.load_sbom(sbom_location)

    sbom_content = json.loads("""
{
    "bomFormat": "CycloneDX",
    "specVersion": "1.4",
    "serialNumber": "urn:uuid:79190df2-cebf-46d1-b651-681b8b7784e3",
    "version": 1,
    "components": [
        {
            "type": "library",
            "author": "Angular Authors",
            "name": "@angular-devkit/architect",
            "version": "0.1303.1",
            "purl": "pkg:apt/@angular-devkit/architect@0.1303.1"
        }
    ]
}
""")

    @mock.patch("hoppr.net.load_url", return_value=sbom_content)
    def test_load_sbom_by_url(self, mock_sbom):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        sbom_location = SBOMRef(url = "http://127.0.0.1/fake-manifest.yml")
        sbom = Manifest.load_sbom(sbom_location)

    sbom_content_bad_spec = json.loads("""
{
    "bomFormat": "CycloneDX",
    "specVersion": "0.0",
    "serialNumber": "urn:uuid:79190df2-cebf-46d1-b651-681b8b7784e3",
    "version": 1,
    "components": [
        {
            "type": "library",
            "author": "Angular Authors",
            "name": "@angular-devkit/architect",
            "version": "0.1303.1",
            "purl": "pkg:apt/@angular-devkit/architect@0.1303.1"
        }
    ]
}
""")

    @mock.patch("hoppr.net.load_url", return_value=sbom_content_bad_spec)
    def test_load_sbom_bad_spec(self, mock_sbom):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        sbom_location = SBOMRef(url = "http://127.0.0.1/fake-manifest.yml")
        with pytest.raises(HopprLoadDataError):
            sbom = Manifest.load_sbom(sbom_location)


    def test_manifest_content_bad_purl(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        with pytest.raises(HopprLoadDataError):
            ManifestFileContent("1.4", "Manifest",
                { "name" : "Manifest", "version" : "1.0", "description" : "A Manifest"}, [], [],
                { "badpurltype" : [ Repository("http://127.0.0.1/bad-purl-type-repo", "Bad Purl Type Repo") ]})
