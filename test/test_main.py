import inspect
import os
from pathlib import Path
import re
import unittest
from unittest import mock
from hoppr.configs.credentials import Credentials
from hoppr.configs.manifest import Manifest
from hoppr.configs.transfer import Transfer
from hoppr.processor import HopprProcessor
from hoppr.result import Result

import pytest
from typer import Exit

import hoppr.main


class TestMain(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def capsys(self, capsys):
        self.capsys = capsys

    def test_version(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        hoppr.main.version()
        captured = self.capsys.readouterr()
        m = re.match(r".*\nHoppr Framework Version: .*\nPython Version: .*", captured.out)
        assert m is not None

    @mock.patch("shutil.copyfile")
    @mock.patch.object(Manifest, 'load_file')
    @mock.patch.object(Credentials, 'load_file')
    @mock.patch.object(Transfer, 'load_file')
    def test_bundle_success(self, mock_transfer_load, mock_cred_load, mock_manifest_load, mock_copyfile):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        hoppr.main.bundle("mock_manifest_file", "mock_creds_file", "mock_transfer_file", Path("mylog.txt"))

    @mock.patch.object(HopprProcessor, 'run', return_value=Result.fail("oops"))
    @mock.patch.object(Manifest, 'load_file')
    @mock.patch.object(Credentials, 'load_file')
    @mock.patch.object(Transfer, 'load_file')
    def test_bundle_fail(self, mock_load_file, mock_cred_load, mock_manifest_load, mock_process_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            hoppr.main.bundle("mock_manifest_file", "mock_creds_file", "mock_transfer_file", None)
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1


    def test_validate(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        hoppr.main.validate([ Path("test","resources","manifest","unit","manifest.yml").resolve()],
            Path("test","resources","credential","cred-test.yml").resolve(),
            Path("test","resources","transfer","transfer-test.yml").resolve())
