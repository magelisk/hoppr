import inspect
import os
from typing import Any
import unittest
from unittest import mock
from hoppr import utils
import hoppr
from pathlib import Path

from hoppr.base_plugins.hoppr import hoppr_rerunner
from hoppr.base_plugins.collector import CollectorPlugin
from hoppr.result import Result
from hoppr.context import Context
from hoppr_cyclonedx_models.cyclonedx_1_4 import Component, Property
from hoppr.hoppr_types.cred_object import CredObject


class TestCollectorBase(unittest.TestCase):
    class MyCollectorPlugin(CollectorPlugin):

        supported_purl_types = ["good1", "good2"]

        def __init__(self, context: Context, config: Any):
            super().__init__(context=context, config=config)
            self.collect_count = 0 # for counting the number of times collect is called

        def get_version(self):
            return "1.70.1"

        @hoppr_rerunner
        def collect(self, comp: Any, repo_url: str, creds: CredObject = None) -> Result:
            self.collect_count += 1
            return Result.success()

    class MyCollectorPluginRetry(CollectorPlugin):

        supported_purl_types = ["good1", "good2"]

        def __init__(self, context: Context, config: Any):
            super().__init__(context=context, config=config)
            self.collect_count = 0 # for counting the number of times collect is called

        def get_version(self):
            return "1.70.1"

        @hoppr_rerunner
        def collect(self, comp: Any, repo_url: str, creds: CredObject = None) -> Result:
            self.collect_count += 1
            return Result.retry(
                "TestCollectorPlugin method collect failed, but might succeed on a subsequent attempt."
            )

    @mock.patch.object(MyCollectorPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collector_base(self, mock_get_repos):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        context = Context(manifest="MANIFEST", collect_root_dir="COLLECTION_DIR", consolidated_sbom="BOM", retry_wait_seconds=1, max_processes=3)
        my_plugin = self.MyCollectorPlugin(context=context, config="CONFIG")

        assert my_plugin.context.collect_root_dir == "COLLECTION_DIR"
        assert my_plugin.config == "CONFIG"
        assert my_plugin.context.manifest == "MANIFEST"

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp=comp)
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
        assert my_plugin.collect_count == 1

        test_dir = my_plugin.directory_for("pypi", "https://www.pypi.org/")
        assert test_dir == str(Path("COLLECTION_DIR","pypi","https%3A%2F%2Fwww.pypi.org"))

        test_dir = my_plugin.directory_for("pypi", "https://www.pypi.org/", subdir="somewhere/deeper")
        assert test_dir == str(Path("COLLECTION_DIR","pypi","https%3A%2F%2Fwww.pypi.org","somewhere","deeper"))

    @mock.patch.object(MyCollectorPluginRetry, "_get_repos", return_value=["https://somewhere.com"])
    def test_collector_base_retry_fail(self, mock_get_repos):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        context = Context(manifest="MANIFEST", collect_root_dir="COLLECTION_DIR", consolidated_sbom="BOM", retry_wait_seconds=1, max_processes=3)
        my_plugin = self.MyCollectorPluginRetry(context=context, config="CONFIG")

        assert my_plugin.context.collect_root_dir == "COLLECTION_DIR"
        assert my_plugin.config == "CONFIG"
        assert my_plugin.context.manifest == "MANIFEST"

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert my_plugin.collect_count == 3

    def test_get_repos(self):
        my_plugin = self.MyCollectorPluginRetry(context="context", config="CONFIG")
        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        comp.properties = []
        comp.properties.append(Property(name="hoppr:repository:component_search_sequence", value='{"version": "v1", "Repositories": ["alpha", "beta", "gamma"]}'))

        assert my_plugin._get_repos(comp) == ['alpha', 'beta', 'gamma']

    @mock.patch.object(MyCollectorPluginRetry, "_get_repos", return_value=["https://somewhere.com"])
    def test_collector_base_no_purl(self, mock_get_repos):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        context = Context(manifest="MANIFEST", collect_root_dir="COLLECTION_DIR", consolidated_sbom="BOM", retry_wait_seconds=1, max_processes=3)
        my_plugin = self.MyCollectorPlugin(context=context, config="CONFIG")

        comp = Component(name="TestComponent", purl=None, type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message == "No purl supplied for component"

    @mock.patch("os.path.isdir", return_value=True)
    @mock.patch("os.path.exists", return_value=True)
    def test_post_stage_process(self,mock_path_exists,mock_isdir):
        context = Context(manifest="MANIFEST", collect_root_dir="COLLECTION_DIR", consolidated_sbom="BOM", retry_wait_seconds=1, max_processes=3)
        my_plugin = self.MyCollectorPlugin(context=context, config="CONFIG")
        collect_result = my_plugin.post_stage_process()
        assert collect_result.is_success()
