import inspect
import unittest
from unittest import mock

from hoppr.core_plugins.collect_maven_plugin import CollectMavenPlugin
from hoppr.configs.credentials import Credentials
from hoppr.result import Result
from hoppr.context import Context
from hoppr.hoppr_types.cred_object import CredObject
from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from test.mock_objects import MockSubprocessRun


class TestCollectorMaven(unittest.TestCase):

    def _create_test_plugin(self):
        context = Context(manifest="MANIFEST", collect_root_dir="COLLECTION_DIR", consolidated_sbom="BOM", retry_wait_seconds=1, max_processes=3)
        my_plugin = CollectMavenPlugin(context=context, config={"maven_command": "mvn", "maven_opts": ["-D1", "-D2"]})
        return my_plugin

    @mock.patch("subprocess.run", return_value=MockSubprocessRun(0))
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @mock.patch.object(CollectMavenPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collect_maven_success(self, mock_get_repos, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:maven/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"

    @mock.patch.object(Credentials, "find_credentials", return_value=CredObject("mock_user_name", "mock_password"))
    @mock.patch("subprocess.run", return_value=MockSubprocessRun(1))
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @mock.patch.object(CollectMavenPlugin, "_get_repos", return_value=["http://somewhere.com"])
    def test_collect_maven_fail(self, mock_get_repos, mock_cmd_check, mock_subprocess_run, mock_get_creds):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:maven/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to download")

    @mock.patch.object(Credentials, "find_credentials", return_value=CredObject("mock_user_name", "mock_password"))
    @mock.patch("subprocess.run", side_effect=[MockSubprocessRun(0), MockSubprocessRun(1), MockSubprocessRun(0), MockSubprocessRun(1), MockSubprocessRun(0), MockSubprocessRun(1)])
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @mock.patch.object(CollectMavenPlugin, "_get_repos", return_value=["http://somewhere.com"])
    def test_collect_maven_fail_pom(self, mock_get_repos, mock_cmd_check, mock_subprocess_run, mock_get_creds):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:maven/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to download")

    def test_get_version(self):
        my_plugin = self._create_test_plugin()
        assert len(my_plugin.get_version()) > 0

    @mock.patch("subprocess.run", return_value=MockSubprocessRun(0))
    @mock.patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.fail("[mock] command not found"))
    @mock.patch.object(CollectMavenPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collect_maven_command_not_found(self, mock_get_repos, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:maven/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message == "[mock] command not found"
