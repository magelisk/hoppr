# Hoppr

---

**Documentation**: <a href="https://hoppr.dev/" target="_blank">https://hoppr.dev/</a>

**Source Code**: <a href="https://gitlab.com/lmco/hoppr/hoppr" target="_blank">https://gitlab.com/lmco/hoppr/hoppr</a>

---

**Hoppr** helps your applications and build dependencies _hop_ between air gapped environments. It is a framework that
supports packaging, transfer, and delivery of dependencies. **Hoppr** relies on the principles of Linux Foundation's focus
on [SPDX](https://spdx.dev/) and the extended functionality of [CycloneDX](https://cyclonedx.org) to define Software
Bill-of-Materials and supply chain management.

**Goals**:

- ```Package``` Framework to collect disparate software products and build dependencies for consolidated packaging
- ```Verify``` Secure Software Supply Chain Management of these dependencies
- ```Transfer``` Abstract the transfer method across environment boundaries
- ```Delivery``` Consolidated packages delivered to target repositories

**Key Features**:

- Standardized workflow
- Extendable With plugins
- Core plugins for common operations

## Install
Install [hoppr from PyPI](https://pypi.org/project/hoppr/).
```
pip install hoppr
```

## Links
- **(TBD)** Getting Started
- **(TBD)** Examples
- **(TBD)** Customizing
- **(TBD)** Questions
- **(TBD)** Contributing
