# Development

## Gitpod
Hoppr is setup with Gitpod to simplify contributions. Use the following button if you would
like to develop and test without setting up a local development environment.

This web-based development environment has:

- A terminal with `poetry` and required python packages for development
- A terminal running `mkdocs serve` and serving the docs in a separate browser window

[Launch Hoppr in Gitpod](https://gitpod.io/#https://gitlab.com/lmco/hoppr/hoppr/-/tree/main/){ .md-button }

## Setup
Install [poetry](https://python-poetry.org/) to simplify package management and building.

<div class="termy">

```console
// Install poetry and dependencies
$ pip install poetry
$ poetry install
```

</div>

## Testing
Any changes to the project need to pass three tests.  Our GitLab pipelines will also verify this during an MR.

New functionality should also be unit tested.

<div class="termy">

```console
// Run unit tests, formating cleanup, and test pep8
$ poetry run pytest ./hoppr
$ poetry run black ./hoppr
$ poetry run pylint ./hoppr
```

</div>

This project also uses `pre-commit` for identifying simple issues before submission to code review.  
You can run `pre-commit run --all-files` when your files are staged to ensure code consistency, but hooks are run on every commit automatically as well.  
You can view the full list of hooks run in [.pre-commit-config.yaml](https://gitlab.com/lmco/hoppr/hoppr/-/blob/main/.pre-commit-config.yaml).

## Docs
The documentation uses [MkDocs](https://www.mkdocs.org/).

All the documentation is in Markdown format in the `./docs` directory.

Please update the documentation after making functionality changes that modify the user experience.
