# Releases

## Versioning

`Hoppr` uses trunk based development paired with semantic releases. All merges
to [main](https://gitlab.com/lmco/hoppr/hoppr/-/tree/main) result in a new release.

The versioning of `major.minor.patch` is handled with [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)
and [semantic-release](https://github.com/semantic-release/semantic-release) in the gitlab pipeline.

Please use [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) to indicate 
what has changed in merge requests. 

Examples:

- `fix(docs): Corrected package urls supported`
- `feat(plugin): Implemented new required argument to plugin interfaces`

## Releasing to Pypi

`Hoppr` releases are pushed to pypi.org outside of this project. A separate pipeline completes outside validation,
builds, and deploys the package.
