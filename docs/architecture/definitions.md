
- ### Input Files
    - `Credentials` - A Hoppr input file that defines what environment variable credentials map to URIs for downloading Manifests, SBOMs, and Components. [Credentials Documentation](/getting_started/inputs#credentials)
    - `Manifest` - A Hoppr input file that defines the "what" and "where" to process and transfer. [Manifest Documentation](/getting_started/inputs#manifests)
    - `Transfer` - A Hoppr input file that defines the "processing" that occurs as part of the transfer through a series of stages and plugins. [Transfer Documentation](/getting_started/inputs#transfers)

- ### Hoppr Vernacular
  - `SBOM`                    :: Software Bill of Material as defined by the [CycloneDX Specification](https://cyclonedx.org/specification/overview)
  - `Combined SBOM`              :: SBOM object for the bundle, showing the data and information for what is bundled for transfer.