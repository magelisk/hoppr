**Hoppr** utilizes four input files which serve different purposes.

### CycloneDX SBOMs
- [CycloneDX](https://cyclonedx.org/){target=_blank} standard is an SBOM format we're using to define `Software Bill of Materials`.
- Components in the SBOMs define __"what"__ gets transferred.
- SBOM files are referenced by a `Manifest` YAML file to allow multiple deliverables to be combined into a single product.

### Manifests
- A **Hoppr** specific YAML file that specifies product dependencies required for product deployments.
- Could be provided by product teams for use by end customers.
- **Hoppr** currently supports the following [Package URLs](https://github.com/package-url/purl-spec/blob/master/PURL-TYPES.rst){target=_blank}
(PURLs) to find components. In `1.0.0` the following package types will be supported:
    - `docker` - container images
    - `git` - git repositories and helm charts
    - `rpm` - redhat RPMs (yum/dnf)
    - `pypi` - python packages
    - `maven` - maven packages
    - `generic` - stand-alone files (e.g. binary) not handled by a package manager
    - `helm` - helm charts

```yaml
---
schemaVersion: v1
kind: manifest

metadata:
  name: string
  version: string
  description: string

sboms:
  - url: string
  - local: string

includes:
  - url: string
  - local: string

repositories:
  <purl type>:
    - url: string
      description: string
```


### Credentials
- A **Hoppr** specific YAML file that defines how to find credentials to access `Manifest`, `SBOMs`, and the components that will be collected.

```yaml
---
schemaVersion: v1
type: authentication

metadata:
  name: string
  version: string
  description: string

auth_required_services:
   - url: string

     user: string
     .. or ...
     user_env: string

     pass_env: string
```


### Transfers
- A **Hoppr** specific YAML file that defines how to run a series of stages and plugins used to collect, process, and bundle components.

```yaml
---
schemaVersion: v1
kind: transfer

stages:
  Collect:
    - name: "hoppr.core_plugins.collect_docker_plugin"
    - name: "hoppr.core_plugins.collect_git_plugin"
    - name: "hoppr.core_plugins.collect_helm_plugin"
    - name: "hoppr.core_plugins.collect_maven_plugin"
    - name: "hoppr.core_plugins.collect_pypi_plugin"
      config:
        pip_command: pip3
    - name: "hoppr.core_plugins.collect_raw_plugin"
  Bundle:
    - name: "hoppr.core_plugins.bundle_tar"
      config:
        tarfile_name: ~/tarfile.tar.gz

max_processes: 3
```


## Example Hoppr Project

This is a simplified example to illustrate the input files and the relationship between them.


```mermaid
graph LR

subgraph Product B
    manifest-b.yml --> sbom-b1.json
    manifest-b.yml --> sbom-b2.json
end

subgraph Product A
    manifest-a.yml --> sbom-a.json
end

subgraph Hoppr Input Files
    manifest.yml --> manifest-a.yml
    manifest.yml --> manifest-b.yml
    transfer.yml
    credentials.yml
end
```


In this example, the root `manifest` references manifests for two other products.


### Schemas

All input file schemas are in JSON as YAML can be converted to JSON and validated against the schema.

Example schema validation using [yq](https://mikefarah.gitbook.io/yq){target=_blank} and [jsonschema](https://pypi.org/project/jsonschema/){target=_blank}


```console
$ cat airgapped.yml | yq eval -P -o json > airgap-manifest.json
$ curl "https://gitlab.com/api/v4/projects/34748703/packages/generic/schemas/v1/hoppr-manifest-schema-v1.json" \
  -o manifest-schema.json

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  100  3205  100  3205    0     0    436      0  0:00:07  0:00:07 --:--:--   740

$ jsonschema --instance airgap-manifest.json manifest-schema.json
$ echo $?
0
```


#### Schema Links

- [manifest json schema](https://gitlab.com/api/v4/projects/34748703/packages/generic/schemas/v1/hoppr-manifest-schema-v1.json){target=_blank}
- [credentials json schema](https://gitlab.com/api/v4/projects/34748703/packages/generic/schemas/v1/hoppr-credentials-schema-v1.json){target=_blank}
- [transfers json schema](https://gitlab.com/api/v4/projects/34748703/packages/generic/schemas/v1/hoppr-transfers-schema-v1.json){target=_blank}

### SBOMs & Manifests
- __Product A__ contains a CycloneDX `SBOM` on the "as-built" components that need to be transferred and their build dependencies.
- __Product B__ contains two CycloneDX `SBOMs` for two different components that need to be transferred and their build dependencies.
- Both Products have `Manifest` files to specify what `SBOMs` are needed for the product to work. Each manifest also specifies a list of repositories to be searched for components specified in their `SBOMs`.
- Lastly, the third party has a `Manifest` that has either local or URL includes of Product A and Product B's Manifests, but does not include any `SBOMs` directly.

We recommend using a tool like [renovate](https://github.com/renovatebot/renovate) to keep your source projects up-to-date, and include the generation of `Manifest` and `SBOM` files in your continuous delivery pipeline(s).  In this way, any transfers that are made with **Hoppr** can be kept current as well.

### Transfer
The `Transfer` file will specify the processing steps needed to collect, augment, filter, report, and bundle components.

### Credentials
The `Credentials` file specifies any credentials needed to access the `Manifest`, `SBOMs`, or `Components` that are access controlled.
