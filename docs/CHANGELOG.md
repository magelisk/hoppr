## [1.0.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.0.0...v1.0.1) (2022-10-05)


### Bug Fixes

* remove 'Starting' messages for skip processes from log ([ec5c874](https://gitlab.com/lmco/hoppr/hoppr/commit/ec5c874a21adff6e77e881dd908bbbedcabc2a5a))

## [1.0.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.4...v1.0.0) (2022-09-29)


### ⚠ BREAKING CHANGES

* Declare v1.0.0

### Features

* Declare v1.0.0 ([31efe26](https://gitlab.com/lmco/hoppr/hoppr/commit/31efe2693ca7060882425d1da9f2a5271b371999))

## [0.20.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.3...v0.20.4) (2022-09-29)


### Bug Fixes

* pass credentials properly from CollectRawPlugin ([6dec417](https://gitlab.com/lmco/hoppr/hoppr/commit/6dec417910e15d7c6e3c865adbca575b26508580))

## [0.20.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.2...v0.20.3) (2022-09-28)


### Bug Fixes

* revert collectyumplugin to 0.20.1 state ([0d7169b](https://gitlab.com/lmco/hoppr/hoppr/commit/0d7169b98607e9ac52516834aefeab7f2de297ee))

## [0.20.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.1...v0.20.2) (2022-09-22)


### Bug Fixes

* creds object None handling ([bdf60f3](https://gitlab.com/lmco/hoppr/hoppr/commit/bdf60f33570528eb1f717fe77d83a69e4e35bb41))
* decode command output ([8ff96a6](https://gitlab.com/lmco/hoppr/hoppr/commit/8ff96a60e7ea0050192304dca42e5b3f569be385))
* fail if stdout empty ([cac2383](https://gitlab.com/lmco/hoppr/hoppr/commit/cac238347fc3681aa059a5a245d33d61900c87ba))
* lint suggestions ([a6c50a6](https://gitlab.com/lmco/hoppr/hoppr/commit/a6c50a6287f4addefc68bc5419c822d034c570ea))
* missing import ([0462ad5](https://gitlab.com/lmco/hoppr/hoppr/commit/0462ad56a1a5f92606260e8e4ec4e0898cdeb221))

## [0.20.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.0...v0.20.1) (2022-09-22)


### Bug Fixes

* always keep test-bundle log as artifact ([4ebb67e](https://gitlab.com/lmco/hoppr/hoppr/commit/4ebb67e40a88b49a8d6a7b0e832557c316f2202b))
* helm collector tries twice, with and without directory in --repo ([e7963cc](https://gitlab.com/lmco/hoppr/hoppr/commit/e7963cc99f189b38d859267cf8757d3c924cf15f))

## [0.20.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.9...v0.20.0) (2022-09-20)


### Features

* move logging to file, give limited feedback to stdout ([bd6a180](https://gitlab.com/lmco/hoppr/hoppr/commit/bd6a180f4d2f47319e36374ba25862dd36842c12))


### Bug Fixes

* add bundle command line option for log file location ([5e11a59](https://gitlab.com/lmco/hoppr/hoppr/commit/5e11a596f53308034e4bb517ee47663df86cabc2))
* add display message on stage failure ([ccc1643](https://gitlab.com/lmco/hoppr/hoppr/commit/ccc164368383941a6fa50e1b06a352eb71e47e82))
* linting again ([d701e38](https://gitlab.com/lmco/hoppr/hoppr/commit/d701e38ed4aea5bff047b3cf2013110bba9e3a8d))
* linting issue ([2d10ae8](https://gitlab.com/lmco/hoppr/hoppr/commit/2d10ae858270ba52586bb8778907a27efe3bdace))
* linting/comments ([afcce3c](https://gitlab.com/lmco/hoppr/hoppr/commit/afcce3cf539e4328c0141757cb8210c178f79c92))
* rename log_fn to logfile_location ([940dfa7](https://gitlab.com/lmco/hoppr/hoppr/commit/940dfa746b20505e847b19cc3f99fbc493fcd86d))

## [0.19.9](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.8...v0.19.9) (2022-09-20)


### Bug Fixes

* font and search bar fix ([f4768ac](https://gitlab.com/lmco/hoppr/hoppr/commit/f4768aca7fa3901abfaf9181cd62cc366ffff991))

## [0.19.8](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.7...v0.19.8) (2022-09-15)


### Bug Fixes

* hoppr docs bug fixes ([b391610](https://gitlab.com/lmco/hoppr/hoppr/commit/b391610af572b79ca0d8229336ebfbced6ac29d6))

## [0.19.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.6...v0.19.7) (2022-09-08)


### Bug Fixes

* add gcr.io as docker repo ([d91e927](https://gitlab.com/lmco/hoppr/hoppr/commit/d91e927d946a9e35d02761c0a9cd781ebcb583fd))
* add integration test manifests, fix gitignore ([e816a0f](https://gitlab.com/lmco/hoppr/hoppr/commit/e816a0fed8d1777bc0e670185788fd81a81645d0))
* add registry.gitlab.com repo for docker ([50d4c34](https://gitlab.com/lmco/hoppr/hoppr/commit/50d4c34c9919e7e05cee9e7078b74a781769680a))
* add test cases, see what happens ([e91d1c4](https://gitlab.com/lmco/hoppr/hoppr/commit/e91d1c42621b183a4d2dabe5046055f5a6a4d4a9))
* always build images ([86f8767](https://gitlab.com/lmco/hoppr/hoppr/commit/86f8767097f50284bb2681a64d3e477dc8e13de0))
* bom locations in manifests ([f611c03](https://gitlab.com/lmco/hoppr/hoppr/commit/f611c03d764c554ab5454758907915af01cdbf3b))
* Correct paths ([b5ada75](https://gitlab.com/lmco/hoppr/hoppr/commit/b5ada756684fccd74c02762a8d4d6e80d12a52c5))
* Create docker specific Boms ([e678f72](https://gitlab.com/lmco/hoppr/hoppr/commit/e678f721fe70050ec71067be4834ed7a551ede6f))
* debug ([e1e9afa](https://gitlab.com/lmco/hoppr/hoppr/commit/e1e9afa798e76b594c09dbd70f560f7565249105))
* debug ([de82165](https://gitlab.com/lmco/hoppr/hoppr/commit/de8216540de830fb25ba12d05382fcc9e3ebbdd3))
* debug looking for tar ([4122825](https://gitlab.com/lmco/hoppr/hoppr/commit/4122825ce2313947973a7e0fd4d1e946752f9ba6))
* debug python tar download ([00fdee8](https://gitlab.com/lmco/hoppr/hoppr/commit/00fdee8715f85d9c7d499e7c0e52834d00d2414b))
* debug python tar download ([f6fce33](https://gitlab.com/lmco/hoppr/hoppr/commit/f6fce3394400ccec616efd960a64130a118b840e))
* First steps on int tests ([bda2934](https://gitlab.com/lmco/hoppr/hoppr/commit/bda293443e879ad9454e54cba8f48d08db414053))
* helm tarfile location, git projects ([d4c3f2f](https://gitlab.com/lmco/hoppr/hoppr/commit/d4c3f2fa0f4fc3f8be3f1192b0fe288596f8dae9))
* hoppr install ([bd8d317](https://gitlab.com/lmco/hoppr/hoppr/commit/bd8d317b8f07e6ef4e98114ad783bf5619add201))
* install tar ([c68ebdf](https://gitlab.com/lmco/hoppr/hoppr/commit/c68ebdf9b29d5d1be992d8558061f036aa5216cc))
* install xz ([ba9c4b6](https://gitlab.com/lmco/hoppr/hoppr/commit/ba9c4b6ab5b6adc0ff3707ee450f51effb1bc71b))
* mod dockerfile to get docker build to run ([d69d3b3](https://gitlab.com/lmco/hoppr/hoppr/commit/d69d3b3e1d8b311cc34aa0dca932d34a58eaaa5a))
* registry ([e2d0a98](https://gitlab.com/lmco/hoppr/hoppr/commit/e2d0a98055edee5c48f1afea468556c1614abb26))
* registry reference ([3846682](https://gitlab.com/lmco/hoppr/hoppr/commit/38466824931368bff7a20b36b8422865786880db))
* remove docker-builder tag ([00fa050](https://gitlab.com/lmco/hoppr/hoppr/commit/00fa05052136aacbc544884b75672ab88c12fcc1))
* Remove monster Renovate Image from test ([e59009d](https://gitlab.com/lmco/hoppr/hoppr/commit/e59009d709d95df551ffebc572423a8c3599d82c))
* rules syntax ([8ccb5c9](https://gitlab.com/lmco/hoppr/hoppr/commit/8ccb5c9b7406cef5345f17fa237b8af5998f8efb))
* run integration tests only on main, cleanup ([6d43357](https://gitlab.com/lmco/hoppr/hoppr/commit/6d43357ccc42a36db7ed24659d8e8c257b061ed2))
* Test GRC ([326a028](https://gitlab.com/lmco/hoppr/hoppr/commit/326a02876bc23ea348dadc2b1d0fa6e4c2af0b5b))
* touch dockerfile ([f8806f3](https://gitlab.com/lmco/hoppr/hoppr/commit/f8806f39ce0f981d5c6453b8cc2744b3fb9d5f42))
* try rules again ([1b66fe5](https://gitlab.com/lmco/hoppr/hoppr/commit/1b66fe5dd14851f6695cac344b1937551596235d))
* try rules changes ([28896f6](https://gitlab.com/lmco/hoppr/hoppr/commit/28896f6fc43771385a2c768f7f53358b75231be3))
* turn off rules for docker build ([76a164c](https://gitlab.com/lmco/hoppr/hoppr/commit/76a164c74ae8371d40f08a4f63f5635fefbc2460))
* typo ([9edb4e3](https://gitlab.com/lmco/hoppr/hoppr/commit/9edb4e37b54eec8581fbde2fa1f29eec9f634369))
* typo in dockerfile name ([7169c3a](https://gitlab.com/lmco/hoppr/hoppr/commit/7169c3a28e7727d2e5fd54c4db93d144026cc8e1))
* update gitlab-ci to run docker build ([b9c2e06](https://gitlab.com/lmco/hoppr/hoppr/commit/b9c2e06a97f1fd19ee5bc19e353d60140560eaa9))

## [0.19.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.5...v0.19.6) (2022-09-01)


### Bug Fixes

* add second append of maven_opts ([4485266](https://gitlab.com/lmco/hoppr/hoppr/commit/4485266dba3a9d39e2aa4e1d6c7069b0cce2d9c0))

## [0.19.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.4...v0.19.5) (2022-09-01)


### Bug Fixes

* add maven options to collect_maven config ([bdbdfbe](https://gitlab.com/lmco/hoppr/hoppr/commit/bdbdfbe6a82a7dde5c366efe99a50690db779f87))

## [0.19.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.3...v0.19.4) (2022-08-31)


### Bug Fixes

* docs updates for short switches ([9aec204](https://gitlab.com/lmco/hoppr/hoppr/commit/9aec204c3fde79baec24f953ebec2dc2413d4a40))

## [0.19.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.2...v0.19.3) (2022-08-31)


### Bug Fixes

* deal with empty namespace in purl ([a382277](https://gitlab.com/lmco/hoppr/hoppr/commit/a38227751b2aeda72a45168fafc984f16e405a24))

## [0.19.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.1...v0.19.2) (2022-08-29)


### Bug Fixes

* Add basic tests for generator ([b37009b](https://gitlab.com/lmco/hoppr/hoppr/commit/b37009b663697d3b118d5b042b4de9dff9ef0cb4))
* Add basic tests for generator ([4b95f30](https://gitlab.com/lmco/hoppr/hoppr/commit/4b95f304c80336a5760ae666fd4fb156bca9a017))
* Add changes per MR 119 ([6e5a126](https://gitlab.com/lmco/hoppr/hoppr/commit/6e5a126363498d2d11b55b07c45bed4e627ebcf9))
* Add components ([9391c43](https://gitlab.com/lmco/hoppr/hoppr/commit/9391c437a291cfda8bbb0e4671089bd62048d503))
* Add in generation code ([76cb41e](https://gitlab.com/lmco/hoppr/hoppr/commit/76cb41e2b510c11c527fbf6df3d5c36ab7026e4e))
* Add sboms switch for feedback and updates per MR comments ([75b1059](https://gitlab.com/lmco/hoppr/hoppr/commit/75b10590a9b2e621df70b7b8637524a2ce21e64d))
* Bump hoppr cdx version ([96ec59b](https://gitlab.com/lmco/hoppr/hoppr/commit/96ec59bc74f033282614cd7f71179898849b38c4))
* Bump poetry version ([57a434e](https://gitlab.com/lmco/hoppr/hoppr/commit/57a434e10cb1061e6c524fe0e66e1fd8f75857f0))
* Bump poetry version ([535b773](https://gitlab.com/lmco/hoppr/hoppr/commit/535b7734ce6c4c4edeef36066229dbebe5be3da6))
* Bump the model version ([4cf9d82](https://gitlab.com/lmco/hoppr/hoppr/commit/4cf9d82b4936390887c8b7124b765a0762d4faa5))
* Clean up comments ([f36cd54](https://gitlab.com/lmco/hoppr/hoppr/commit/f36cd547e9fcaf04384bd92274e0261cf8b2aefc))
* Clean up comments ([d1b0020](https://gitlab.com/lmco/hoppr/hoppr/commit/d1b0020ba70cf5dacd6f64498e29ffa3de5bf91f))
* Clean up per mr review ([450cddc](https://gitlab.com/lmco/hoppr/hoppr/commit/450cddc92db28d7d52f6b64b83749faedbaa0805))
* Clean up print statement ([f12b817](https://gitlab.com/lmco/hoppr/hoppr/commit/f12b8179ec19d9f5901a17fd931aa92a8950c832))
* code review comment, use cpu_count for default max_processes ([bd3a9a3](https://gitlab.com/lmco/hoppr/hoppr/commit/bd3a9a3aa768e8010501e21479f65d933463d6e9))
* Correct bom typeerror ([7fa342c](https://gitlab.com/lmco/hoppr/hoppr/commit/7fa342cbf28d759c3a2eaa662a7a2c1f0a7eae39))
* Correct errors ([08253c5](https://gitlab.com/lmco/hoppr/hoppr/commit/08253c5fe6ccae181f3a8c7622106a6d6b92c5ea))
* Correct invalid escape sequence ([73f4e2d](https://gitlab.com/lmco/hoppr/hoppr/commit/73f4e2d9a4064d7deeec1e906a82cc7699998749))
* Correct lockfile ([4062829](https://gitlab.com/lmco/hoppr/hoppr/commit/4062829d014ec15e4d0954a3f7afef7651fa664e))
* Correct poetry lock file ([a199a90](https://gitlab.com/lmco/hoppr/hoppr/commit/a199a9003d736f05bfc6f7f3e0564b4691a1cc32))
* Correct the test errors ([ed967b8](https://gitlab.com/lmco/hoppr/hoppr/commit/ed967b8ec5b30ee88d11291b5be2f7bd77067081))
* Correct the test errors ([71b82cd](https://gitlab.com/lmco/hoppr/hoppr/commit/71b82cd08eac31af790fe103b6fc76144646f797))
* Correct the test errors ([20e9d8e](https://gitlab.com/lmco/hoppr/hoppr/commit/20e9d8edc4b69da7e2e500cb1b2c75ec26b38d5c))
* Correct variable names ([41b093a](https://gitlab.com/lmco/hoppr/hoppr/commit/41b093a0fd4281e1724d38f7249309fdb0c02d29))
* Handle schema tag ([0ee39fa](https://gitlab.com/lmco/hoppr/hoppr/commit/0ee39fae889f887ab4e2d4305649586cbc6c0202))
* Handle schema tag ([0562d39](https://gitlab.com/lmco/hoppr/hoppr/commit/0562d39dbc7c0a4818b5bb9163ab7cfb909971e5))
* Merge ([b278fff](https://gitlab.com/lmco/hoppr/hoppr/commit/b278fffb71b42f52eb80319194c7dded2f1bc026))
* Merge ([0ea5af4](https://gitlab.com/lmco/hoppr/hoppr/commit/0ea5af46b42ef657e31223f1f7630b31dd729a12))
* Merge main into branch ([26bb6fa](https://gitlab.com/lmco/hoppr/hoppr/commit/26bb6fa52259616dab6823302ddffc7ebdc447b3))
* Merge main into branch ([c32b43e](https://gitlab.com/lmco/hoppr/hoppr/commit/c32b43ebacec8cebaa8a2c0bdb65a819939cbd60))
* put max_processes in Context, use max_processes from config file, fix bug in remove_empty utility ([ab9e09b](https://gitlab.com/lmco/hoppr/hoppr/commit/ab9e09b9d4137f70ff56bde043252d5930cd2533))
* Remove credentials default file ([7fccf29](https://gitlab.com/lmco/hoppr/hoppr/commit/7fccf29aa220b8aa52621dedf92ae3c4f8174130))
* Remove exclude ([ea07ca3](https://gitlab.com/lmco/hoppr/hoppr/commit/ea07ca3dd5ef81485040e636bdf020028c222739))
* Remove values.yml and update main.py ([27a97ee](https://gitlab.com/lmco/hoppr/hoppr/commit/27a97ee31efe5cc74a6b4d70a784763c6822119b))
* typo ([63fc7c2](https://gitlab.com/lmco/hoppr/hoppr/commit/63fc7c2ad682f05461548e0f8b2d9b3d74d476a9))
* Update credentials variable to HOPPR_CREDS_CONFIG to match validate env ([d402167](https://gitlab.com/lmco/hoppr/hoppr/commit/d402167250ad10d36ddec9a9a1cff5f16ca165ec))
* Update git collector to support gitlab, github, and golang purl types ([0f3b258](https://gitlab.com/lmco/hoppr/hoppr/commit/0f3b25871b57467a65594d69aa2b72fcca3cfe7f))
* Update per MR comments ([75cd885](https://gitlab.com/lmco/hoppr/hoppr/commit/75cd885037c2c02e65dde279fb6ccb589920320f))
* Update poetry lock ([ad57383](https://gitlab.com/lmco/hoppr/hoppr/commit/ad57383d8a6307187709a9f33e56ff9fec9362f7))
* Update poetry lock file ([ee79895](https://gitlab.com/lmco/hoppr/hoppr/commit/ee79895933ab14ac1078f2ed02c2477013044288))
* Update purl_type.py to include new types ([f208be0](https://gitlab.com/lmco/hoppr/hoppr/commit/f208be0deec909d9aa2a22d98d6aaed18f43b815))
* Updates per mr review ([1f2e6d9](https://gitlab.com/lmco/hoppr/hoppr/commit/1f2e6d9b10e05077ea1a0e65d1e3e73cf1048451))
* Write yaml test ([0b21526](https://gitlab.com/lmco/hoppr/hoppr/commit/0b21526ce81d6d06cc7d17349f1fa05f21415ecd))

## [0.19.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.0...v0.19.1) (2022-08-29)


### Bug Fixes

* bug when deleting docker file for retry ([a4b1140](https://gitlab.com/lmco/hoppr/hoppr/commit/a4b1140812a68db7c49f6962dd1d0312442d7f28))

## [0.19.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.19...v0.19.0) (2022-08-25)


### Features

* add vscode tasks for hoppr docs ([0af74df](https://gitlab.com/lmco/hoppr/hoppr/commit/0af74df27097cabcc5ded7d0eb626abe8634cd5f))


### Bug Fixes

* add clarifying note yum/dnf ([f2cb08b](https://gitlab.com/lmco/hoppr/hoppr/commit/f2cb08baccf847af6a71de4e99fc896d8ba6b244))
* definitions ([afaead7](https://gitlab.com/lmco/hoppr/hoppr/commit/afaead72c215de5b288a541855cf246fea17ae58))
* MR updates ([9709376](https://gitlab.com/lmco/hoppr/hoppr/commit/9709376b3d275200f4dfb9ae88a0f4d0f24b342f))
* Updating docs for MVP alignment ([c6ecdf0](https://gitlab.com/lmco/hoppr/hoppr/commit/c6ecdf0d7f10d85157fb3824022d0d968643fdcf))

## [0.18.19](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.18...v0.18.19) (2022-08-16)


### Bug Fixes

* allow cyclonedx models to update ([857659d](https://gitlab.com/lmco/hoppr/hoppr/commit/857659d1c3271b05c835fa656d6d952f58d083bb))

## [0.18.18](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.17...v0.18.18) (2022-08-15)


### Bug Fixes

* minor bug resulting in repeated failure messages in summary ([1b0c68d](https://gitlab.com/lmco/hoppr/hoppr/commit/1b0c68dc1ba5d11d568211730619d48221170c82))

## [0.18.17](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.16...v0.18.17) (2022-08-11)


### Bug Fixes

* confusing test purl ([a9937ed](https://gitlab.com/lmco/hoppr/hoppr/commit/a9937ed4a4918195ee1c1bb48d722d21db13c0e4))
* Yum collector directories ([6127730](https://gitlab.com/lmco/hoppr/hoppr/commit/6127730f27aeb4d847e913b08c1199ae307f2b95))

## [0.18.16](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.15...v0.18.16) (2022-08-04)


### Bug Fixes

* Changed hoppr_types to utilize hoppr-cyclonedx-models ([9648618](https://gitlab.com/lmco/hoppr/hoppr/commit/9648618fba1e87b6f396f5b6093e9aab2872020b))
* Changed imports that used hoppr_types to use oppr_cyclonedx_models module ([24f7f73](https://gitlab.com/lmco/hoppr/hoppr/commit/24f7f73860abdfa8153c65f76959bdb71a447afa))
* Changed types to hoppr_types, poetry add hoppr_cyclonedx ([6ebba64](https://gitlab.com/lmco/hoppr/hoppr/commit/6ebba645a4090b42c3af90c54a6289b095237463))
* deleted excess cyclonedx files ([f28fcae](https://gitlab.com/lmco/hoppr/hoppr/commit/f28fcae7c27332b65379edd8803b97b1479c92b2))
* modified files using old 'types' directory ([5a87731](https://gitlab.com/lmco/hoppr/hoppr/commit/5a8773131378e6e4fc484010b9697e6790c3707c))
* modify 'type' in pylintrc ([f6c40d2](https://gitlab.com/lmco/hoppr/hoppr/commit/f6c40d24f89844362436deff415b800a6807743f))
* modify files with old type naming ([3541522](https://gitlab.com/lmco/hoppr/hoppr/commit/3541522c9b6539b3c746890c459497e519df7255))

## [0.18.15](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.14...v0.18.15) (2022-08-02)


### Bug Fixes

* typo ([2bf4ec1](https://gitlab.com/lmco/hoppr/hoppr/commit/2bf4ec10a6228753c71b5143c962df64921ca4e1))
* Update git collector to support gitlab, github, and golang purl types ([b244739](https://gitlab.com/lmco/hoppr/hoppr/commit/b24473991b6d82e99045121a8b0796033956d173))
* Update purl_type.py to include new types ([f874516](https://gitlab.com/lmco/hoppr/hoppr/commit/f8745168900155da42551ce70d052db6256af42b))

## [0.18.14](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.13...v0.18.14) (2022-07-27)


### Bug Fixes

* code review comment, use cpu_count for default max_processes ([d347282](https://gitlab.com/lmco/hoppr/hoppr/commit/d3472828ab8d697820f5843420fc5da46e7b017f))
* put max_processes in Context, use max_processes from config file, fix bug in remove_empty utility ([4b47397](https://gitlab.com/lmco/hoppr/hoppr/commit/4b473979bb927d650e392506c33590d8576dbb95))

## [0.18.13](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.12...v0.18.13) (2022-07-26)


### Bug Fixes

* Hotfix pages urls ([6bb1083](https://gitlab.com/lmco/hoppr/hoppr/commit/6bb10838e86a6c3f64f52441f26aed4b72fd995e))

## [0.18.12](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.11...v0.18.12) (2022-07-26)


### Bug Fixes

* Clean up darkmode ([823c8e0](https://gitlab.com/lmco/hoppr/hoppr/commit/823c8e096cbccbb6a792542e5f9a2924c08c8361))
* Correct overview path ([3a62b12](https://gitlab.com/lmco/hoppr/hoppr/commit/3a62b120e59a660a082ae617adf7c9f614ed3422))
* Delete bad image ([9686c62](https://gitlab.com/lmco/hoppr/hoppr/commit/9686c62e074ceb01cfb128b4ca6ffe3358eab796))
* Test out darkmode ([a70f355](https://gitlab.com/lmco/hoppr/hoppr/commit/a70f355da766d21b083587e9898afc93a1e1531a))

## [0.18.11](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.10...v0.18.11) (2022-07-21)


### Bug Fixes

* move logging to new class, needed for reuse in plug-ins ([6a49e33](https://gitlab.com/lmco/hoppr/hoppr/commit/6a49e33a3e512247f056b112e1167c53d7e473a8))

## [0.18.10](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.9...v0.18.10) (2022-07-19)


### Bug Fixes

* Add missing init to test module ([5e981d8](https://gitlab.com/lmco/hoppr/hoppr/commit/5e981d84a9720ef9be92d70c94161ee83d215bac))
* Add vscode debug support; tasks/launch.json ([3aa3830](https://gitlab.com/lmco/hoppr/hoppr/commit/3aa3830cc0701e3c24747a8ba7e0f6d760370246))
* Allow untrusted. ([bca89f9](https://gitlab.com/lmco/hoppr/hoppr/commit/bca89f9968149db4f83f40c2beeda1b3276df78e))
* Put --allow-untrusted in the add line. ([13b977f](https://gitlab.com/lmco/hoppr/hoppr/commit/13b977f9ddffb6a5909a775f8edf4e15f1069cd3))
* Try using plus equals ([512109f](https://gitlab.com/lmco/hoppr/hoppr/commit/512109fdbcc28066d42e27d0c45e57539ae0e359))
* trying another different syntax ([127eaef](https://gitlab.com/lmco/hoppr/hoppr/commit/127eaefb27bb6b561e4971ba10f72930bdbc91d3))
* trying different syntax ([4d8675c](https://gitlab.com/lmco/hoppr/hoppr/commit/4d8675c7894b554fbd67491b98337d942d446b44))
* trying yet another different syntax ([57292be](https://gitlab.com/lmco/hoppr/hoppr/commit/57292bebdb7e0e5c811e321690f6883638ce3c4a))
* Use correct yq version ([c89061a](https://gitlab.com/lmco/hoppr/hoppr/commit/c89061aeb9decb67a29e62e7c99242a6665b08c9))
* yq issue, added reason to an error log message ([2434066](https://gitlab.com/lmco/hoppr/hoppr/commit/2434066cd476dc45ecd4f4c55ce310f0c62b05ac))
* YQ version ([cb4b6cf](https://gitlab.com/lmco/hoppr/hoppr/commit/cb4b6cfb0a1cd976b0818e92045cc4a6c638bf70))

## [0.18.9](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.8...v0.18.9) (2022-07-11)


### Bug Fixes

* Add artifact attestation ([f5d9133](https://gitlab.com/lmco/hoppr/hoppr/commit/f5d9133d8eb41a1bc108cddc00ea6c098e21f55c))
* Add clean images ([6ff6017](https://gitlab.com/lmco/hoppr/hoppr/commit/6ff601745e42f3bdc1b9bef4aedd9619731c56b5))
* Add in the correct images ([109bf4c](https://gitlab.com/lmco/hoppr/hoppr/commit/109bf4ca55a093297c5392808a19766c58718dbf))
* Added lfs ([1f23c73](https://gitlab.com/lmco/hoppr/hoppr/commit/1f23c73902fd7443006bf2d609770ec6670a0b4f))
* Compress images and add lfs ([6d92cb1](https://gitlab.com/lmco/hoppr/hoppr/commit/6d92cb193eeb50325ebdd507fae32960cebf039c))
* Correct image resolutions ([3c2cfe3](https://gitlab.com/lmco/hoppr/hoppr/commit/3c2cfe38d06b6247deed91e5d30804e5c2bbfc6d))
* Flatten out the svg rgb color ([1b23474](https://gitlab.com/lmco/hoppr/hoppr/commit/1b234749343adbee81dc66e92179a6daef7c04d0))
* Push changes ([f4a5b36](https://gitlab.com/lmco/hoppr/hoppr/commit/f4a5b3634c7d5c9d549ff63ca9e59338cca024fa))
* Remove LFS ([fcc0225](https://gitlab.com/lmco/hoppr/hoppr/commit/fcc0225eeceab7388196d00cfc04472f0c300305))
* Revert ([5eb4c2f](https://gitlab.com/lmco/hoppr/hoppr/commit/5eb4c2fca850f31649249ad2a0feec09b982b70e))
* Rework the images ([847b1a3](https://gitlab.com/lmco/hoppr/hoppr/commit/847b1a3b1830f297332f829539e1e74a3518eeab))
* Speed up the ground scroll a little ([443005b](https://gitlab.com/lmco/hoppr/hoppr/commit/443005bfdc8b7c27b38351f687a565e361d9f35a))
* Test generating metadata ([c5e8b1a](https://gitlab.com/lmco/hoppr/hoppr/commit/c5e8b1ac28c809228195a1d2f0534e5fb4829c48))
* Update names, update more icon ([5054584](https://gitlab.com/lmco/hoppr/hoppr/commit/5054584f3c66bc2aebc1caabf002eeacdba84f78))
* Update package.json ([decfe10](https://gitlab.com/lmco/hoppr/hoppr/commit/decfe10200d38cd3db9b2df66a6657cebb09bd29))

## [0.18.8](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.7...v0.18.8) (2022-07-01)


### Bug Fixes

* Manual merge ([8af13b0](https://gitlab.com/lmco/hoppr/hoppr/commit/8af13b0ba9bd7be79c744d74f9bcc6dbeb5fe7f8))
* MR Cleanup and improved net utils coverage ([29d16fd](https://gitlab.com/lmco/hoppr/hoppr/commit/29d16fdaf9fc9f842d0b718a3c4583285def0004))
* poetry.lock ([79ce411](https://gitlab.com/lmco/hoppr/hoppr/commit/79ce411ec7b6ba9511b56c303e44ed75c0af1b6c))
* Pylint multi-return + test_process ([44fd966](https://gitlab.com/lmco/hoppr/hoppr/commit/44fd96637cb1c8ec23d9d6b34d05a27b59e0dfc7))
* yum test ([2809153](https://gitlab.com/lmco/hoppr/hoppr/commit/28091536787f75f31687369ac5155fd5f432d3e3))

## [0.18.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.6...v0.18.7) (2022-06-29)


### Features

* implement collect_yum_plugin. ([57db6d7](https://gitlab.com/lmco/hoppr/hoppr/commit/57db6d71f1bdc178dd887fdabc7935c2419eef4b))


### Bug Fixes

* address first wave of MR comments. ([134e958](https://gitlab.com/lmco/hoppr/hoppr/commit/134e9586ebc3f9cc5029b307915d4b5ab5ab76f8))
* formatting update from black. ([0cbb2bd](https://gitlab.com/lmco/hoppr/hoppr/commit/0cbb2bdb008f04785a16091bd6ebe4f1a7c81502))
* log whole purl object instead of just name. ([9a315ac](https://gitlab.com/lmco/hoppr/hoppr/commit/9a315ac50d24260848fa8ca8a124befa2d2c473b))

## [0.18.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.5...v0.18.6) (2022-06-28)


### Bug Fixes

* Hotfix the overview reference ([65e9c35](https://gitlab.com/lmco/hoppr/hoppr/commit/65e9c350f3bdf9318deaae03a2f53cb0b2b7d3a9))

## [0.18.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.4...v0.18.5) (2022-06-28)


### Bug Fixes

* Add apt cache and install yarn ([2dccbb1](https://gitlab.com/lmco/hoppr/hoppr/commit/2dccbb101103637f2e4fde2e215774c2435abeef))
* Add core build tools ([be681c2](https://gitlab.com/lmco/hoppr/hoppr/commit/be681c2a286a1ecfa5556e17e4ca49e4231a83de))
* Add in templating for env ([b7b82a6](https://gitlab.com/lmco/hoppr/hoppr/commit/b7b82a632d752415eee59edf21b402cec47cf0e2))
* Add in webp images ([e3532bf](https://gitlab.com/lmco/hoppr/hoppr/commit/e3532bf1e54253e405c322fc65920a41f0c810d7))
* Add longer image to see if it cleans up mobile ([400f449](https://gitlab.com/lmco/hoppr/hoppr/commit/400f4499f8ca570e284d8e3fbe7fe926ea9a4186))
* Add md_in_html for dynamic tags ([0ed4754](https://gitlab.com/lmco/hoppr/hoppr/commit/0ed4754b0593848f3e4651b70107f8a5484e5d18))
* Add semantic dependencies back in ([4b0d5cb](https://gitlab.com/lmco/hoppr/hoppr/commit/4b0d5cb19da42dc95b1ac93dffb92eb7387e00e6))
* Clean up docs ([a22c742](https://gitlab.com/lmco/hoppr/hoppr/commit/a22c7422764b1a426843a24c1a89b84014d15981))
* Clean up headers ([554e02b](https://gitlab.com/lmco/hoppr/hoppr/commit/554e02be75de8902f38cee361fa7f32434eb1ddf))
* Clean up YQ syntax ([1e3b631](https://gitlab.com/lmco/hoppr/hoppr/commit/1e3b631168357ca4fcab4d3da1ec3cddbd4809a9))
* Cleanup hoppr ([b58d63b](https://gitlab.com/lmco/hoppr/hoppr/commit/b58d63bac2a14535bfb3a63a206124034cbd069d))
* Correct install syntax ([6c374dd](https://gitlab.com/lmco/hoppr/hoppr/commit/6c374ddc8937a5133f05c50cb8d35293205b36ab))
* Correct mkdocs ([229e790](https://gitlab.com/lmco/hoppr/hoppr/commit/229e790caa08f37b1e5653b4801e0635a127917d))
* Correct mkdocs ([c50d135](https://gitlab.com/lmco/hoppr/hoppr/commit/c50d13594634fc6c20aff0ff8db91b484717b3e3))
* Correct pages job ([4d6898c](https://gitlab.com/lmco/hoppr/hoppr/commit/4d6898ccc74d06d1ab919ea3e0010b992fc9d79a))
* Correct the link and ref ([124077b](https://gitlab.com/lmco/hoppr/hoppr/commit/124077b3d3dc46675fbf23441e995d00ed141528))
* Correct the link text ([93f80cc](https://gitlab.com/lmco/hoppr/hoppr/commit/93f80cc733c44bf767507f0b5935c9945c66a296))
* Corrected image layout ([32b59d4](https://gitlab.com/lmco/hoppr/hoppr/commit/32b59d406a732cef1bf637429e6617c018a8afc8))
* Expose Docs on MR Request ([0a2149d](https://gitlab.com/lmco/hoppr/hoppr/commit/0a2149df20ea1cf3d28a070e878607e11332915d))
* Merge in ([b0f8330](https://gitlab.com/lmco/hoppr/hoppr/commit/b0f833098c3e5070c4d89d61d70c39cf320c6cea))
* Pass at parallax ([2bd038c](https://gitlab.com/lmco/hoppr/hoppr/commit/2bd038c9f336e6e89c35e94982e08bf0512364d5))
* Pass at parallax ([073ff88](https://gitlab.com/lmco/hoppr/hoppr/commit/073ff889f9ede8906057f20145bcfcdffb766db5))
* Pre-Commit fixes ([d464c79](https://gitlab.com/lmco/hoppr/hoppr/commit/d464c7955768c66822d173585ef78856422ac0bb))
* Pre-Commit fixes ([5ccb1fb](https://gitlab.com/lmco/hoppr/hoppr/commit/5ccb1fb3ed9999231b5ab09d4475008ba8492863))
* Pre-Commit fixes ([ae518a5](https://gitlab.com/lmco/hoppr/hoppr/commit/ae518a57368797527f09a0bc08d2f8b8ec76aaf5))
* Remove yarn.lock ([9a9d3a8](https://gitlab.com/lmco/hoppr/hoppr/commit/9a9d3a8f2f7cb3f0f804c8fc4a7db175578494b6))
* Set default hoppr.version field in mkdocs to pass linting ([2af642a](https://gitlab.com/lmco/hoppr/hoppr/commit/2af642a72362c1312c0bd7552ff4cbc1fce2a991))
* Set hoppr version in docs ([9e27fd1](https://gitlab.com/lmco/hoppr/hoppr/commit/9e27fd1159b59d3ce18a314bb4b7d47a3f88f24a))
* Update gant ([ede4b5d](https://gitlab.com/lmco/hoppr/hoppr/commit/ede4b5d63aec5e6ee68e1f2208e5d5857715d23b))
* Update roadmap to reflect appropriate dates ([309ed33](https://gitlab.com/lmco/hoppr/hoppr/commit/309ed335d004ee6931d55245b0c6d64b45f1ed93))
* We don't have any containers yet ([9a79f46](https://gitlab.com/lmco/hoppr/hoppr/commit/9a79f468845e79e76d84f4fd917c842332abe609))

## [0.18.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.3...v0.18.4) (2022-06-27)


### Bug Fixes

* **deps:** update dependency packageurl-python to ^0.10.0 ([0bdaddf](https://gitlab.com/lmco/hoppr/hoppr/commit/0bdaddf8bcc284e5eb7ab407e067253e32cc47fd))

## [0.18.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.2...v0.18.3) (2022-06-23)

## [0.18.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.1...v0.18.2) (2022-06-22)


### Bug Fixes

* setup versioning to be updated before build, and for publish to only publish (instead of build too). ([c907149](https://gitlab.com/lmco/hoppr/hoppr/commit/c907149d18275a1031c03279f54d1246d49b6782))
* version in pyproject.toml is updated with semantic version ([4347611](https://gitlab.com/lmco/hoppr/hoppr/commit/4347611b752fc336660c7e59d71c0b4074551752))

## [0.18.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.0...v0.18.1) (2022-06-14)


### Bug Fixes

* add message when manifest is not re-loaded ([116b550](https://gitlab.com/lmco/hoppr/hoppr/commit/116b5508bac8c873a1b7847f9f5a143302aa7439))
* potential infinite recursion in manifest load process ([bbe0ac7](https://gitlab.com/lmco/hoppr/hoppr/commit/bbe0ac7fbc938d70036f1fcb7b540ed34215d4e9))
* use resolved path for file checks of loaded_manifests ([20af8f8](https://gitlab.com/lmco/hoppr/hoppr/commit/20af8f8b8d7e675a75194270171c1ee3390137e3))

## [0.18.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.17.1...v0.18.0) (2022-06-13)


### Features

* add metadata to bundles ([778640a](https://gitlab.com/lmco/hoppr/hoppr/commit/778640aa025cf4690a8fdc935aa5c04f56a1c1b1))


### Bug Fixes

* Move consolidated_sbom to context, use Transfer object for transfer config ([6b35ee2](https://gitlab.com/lmco/hoppr/hoppr/commit/6b35ee23a49b4e8442c82ba76db3ce2745e379b3))

## [0.17.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.17.0...v0.17.1) (2022-06-09)


### Bug Fixes

* **deps:** update dependency semantic-release to v19.0.3 ([b5b9bbc](https://gitlab.com/lmco/hoppr/hoppr/commit/b5b9bbc49f773b37f961234a14473b34937d5c31))

## [0.17.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.16.0...v0.17.0) (2022-06-06)


### Features

* add maven copier ([062af40](https://gitlab.com/lmco/hoppr/hoppr/commit/062af405b3ee815377d5191557a4d903d2e51717))


### Bug Fixes

* changed name of test component from 'Bob' to 'TestComponent', for Eric ([1ad4190](https://gitlab.com/lmco/hoppr/hoppr/commit/1ad41900a4bb5a644b21f5dc3f52159047d032e5))
* linting issues ([f041c16](https://gitlab.com/lmco/hoppr/hoppr/commit/f041c16fcf3088aedae09d714fd32e2f12e43145))
* merge from main, update helm collector for run_command ([af6051c](https://gitlab.com/lmco/hoppr/hoppr/commit/af6051cd63e8f21696697e19ebbb3624d367a720))
* use credentials (when appropriate) in maven-plugin, add test files ([28ebe3f](https://gitlab.com/lmco/hoppr/hoppr/commit/28ebe3fcd80b5614bd1904ea09cae9f0a3f70f18))

## [0.16.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.6...v0.16.0) (2022-06-02)


### Features

* helm collector plugin ([ea0a8f7](https://gitlab.com/lmco/hoppr/hoppr/commit/ea0a8f7e843539229b8d4785cf241dadaa0a2324))


### Bug Fixes

* resolving merge request threads ([2851815](https://gitlab.com/lmco/hoppr/hoppr/commit/2851815dd48d76d17ea9c1e6871fa9e602d37e56))
* updated pylint version ([e090888](https://gitlab.com/lmco/hoppr/hoppr/commit/e090888ebfe39e8b35c86bacd78b65689f69891a))

## [0.15.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.5...v0.15.6) (2022-06-01)

## [0.15.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.4...v0.15.5) (2022-06-01)


### Bug Fixes

* Update taxonomy reference in navigation layout ([5fe0399](https://gitlab.com/lmco/hoppr/hoppr/commit/5fe03992158049a7a9377de4711196c1d656764c))

## [0.15.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.3...v0.15.4) (2022-05-31)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.3.2 ([1f8f020](https://gitlab.com/lmco/hoppr/hoppr/commit/1f8f0201372b712105060d59d46adccf9bd4fb13))

## [0.15.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.2...v0.15.3) (2022-05-29)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.3.1 ([0a3e506](https://gitlab.com/lmco/hoppr/hoppr/commit/0a3e506bf6831ab18a809cb624034b28641cf22a))

## [0.15.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.1...v0.15.2) (2022-05-27)


### Bug Fixes

* **deps:** update dependency conventional-changelog-conventionalcommits to v5 ([96de6d3](https://gitlab.com/lmco/hoppr/hoppr/commit/96de6d34134c95b35c79116fd30a6a20bd865bd7))

### [0.15.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.0...v0.15.1) (2022-05-26)


### Bug Fixes

* created a Credentials Object for ease of use ([662d434](https://gitlab.com/lmco/hoppr/hoppr/commit/662d434601beafcff187b0db41bd6f65a240cb33))
* merge from main ([401d93d](https://gitlab.com/lmco/hoppr/hoppr/commit/401d93d551fea1caa2ef0bf67bd897a85b6710e9))
* small edits on pypi collector for CredObject ([85929e9](https://gitlab.com/lmco/hoppr/hoppr/commit/85929e91c873e3ceb71aaf33cadc15c4b6910f76))

## [0.15.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.14.0...v0.15.0) (2022-05-26)


### Features

* add pypi collector, bug fixes, cleanup ([09c7fd8](https://gitlab.com/lmco/hoppr/hoppr/commit/09c7fd8ff85e33fe39a8e28b0ef4e946d7d491f6))


### Bug Fixes

* merge with main, clean-up purl check in git ([da27f11](https://gitlab.com/lmco/hoppr/hoppr/commit/da27f115f653c88dd05e15beb049532b901e527a))
* remove setuptoos installation -- not needed ([b9f3e72](https://gitlab.com/lmco/hoppr/hoppr/commit/b9f3e72097a47e5a1980bccd78aa72c7d44e3d8e))
* Return RETRY rather than FAIL on docker failure ([3634361](https://gitlab.com/lmco/hoppr/hoppr/commit/363436165e1f797008e5d31ec47da3cd00814cfc))

## [0.14.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.6...v0.14.0) (2022-05-25)


### Features

* git collector plugin ([30532c8](https://gitlab.com/lmco/hoppr/hoppr/commit/30532c841488ef81c483062b542c11981f448c26))


### Bug Fixes

* git collector bundling ([a3cf4e2](https://gitlab.com/lmco/hoppr/hoppr/commit/a3cf4e25bbedc4bc3fe5d067d6bea522b5ebb8d7))

### [0.13.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.5...v0.13.6) (2022-05-25)


### Bug Fixes

* Correct syntax ([ea67974](https://gitlab.com/lmco/hoppr/hoppr/commit/ea679741b2faaabd909cb1e71a8e0fd822cfae64))
* Correct the artifacts Report section ([c3890c6](https://gitlab.com/lmco/hoppr/hoppr/commit/c3890c685ab06fe119094897dcf9a8476fea330b))

### [0.13.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.4...v0.13.5) (2022-05-23)


### Bug Fixes

* improved unit test & code coverage ([1e3aca1](https://gitlab.com/lmco/hoppr/hoppr/commit/1e3aca15488e16d2e0eb354be9a3b78e4e924a5e))

### [0.13.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.3...v0.13.4) (2022-05-23)


### Bug Fixes

* initial integration tests, minor fixes ([da26260](https://gitlab.com/lmco/hoppr/hoppr/commit/da26260c817722738e9442cbe45804370c4c2197))
* Merge branch 'man' into eliminate-prototype-code ([e0887c8](https://gitlab.com/lmco/hoppr/hoppr/commit/e0887c8cc39e45171215d4042d218e70971451e4))
* remove print statements ([dd1d85d](https://gitlab.com/lmco/hoppr/hoppr/commit/dd1d85de3d047fc5df07ddca3b57e7e2656a12c7))
* start using credentials from credentials.config, rather than kludges ([541d906](https://gitlab.com/lmco/hoppr/hoppr/commit/541d90623e1af7a218e151cb77ee830174b470dd))
* use actual manifest, flattened sbom ([62c4552](https://gitlab.com/lmco/hoppr/hoppr/commit/62c455234583d2b6b53d84331599378e6f565eff))
* use typer.echo instead of print ([5bf306c](https://gitlab.com/lmco/hoppr/hoppr/commit/5bf306c5a719dca13e531f40135107595c453d5b))

### [0.13.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.2...v0.13.3) (2022-05-18)


### Bug Fixes

* added back generic-manifest-child, fixed tests to reflect it ([d64a524](https://gitlab.com/lmco/hoppr/hoppr/commit/d64a524a18c241191a6426df438996324a36964c))
* code clean-up, test repo property creation ([fd027ab](https://gitlab.com/lmco/hoppr/hoppr/commit/fd027abda4787a30374b055ffd791d48d29ba458))
* comment-out a couple of asserts until generic-child is checked in ([576ac9c](https://gitlab.com/lmco/hoppr/hoppr/commit/576ac9c5164a508edd30063e83c677d995a04560))
* dedup input repositories ([22e639d](https://gitlab.com/lmco/hoppr/hoppr/commit/22e639d1a71145c749787b0a335f0c842b505a19))
* fixed unit tests for flattening ([3f07472](https://gitlab.com/lmco/hoppr/hoppr/commit/3f07472598236864ebfc54ea12663b518c2df177))
* Implement ADR 22, refactoring manifest to use dict of purl_types for repositories ([5f01e6b](https://gitlab.com/lmco/hoppr/hoppr/commit/5f01e6b4e62524b38939b009c8edf2962e6cba06))
* merge from main ([beb5260](https://gitlab.com/lmco/hoppr/hoppr/commit/beb52600dcde6e1bce3c33fcae9802b2ad71902a))
* merge with main ([b82a8e8](https://gitlab.com/lmco/hoppr/hoppr/commit/b82a8e81dde0acc716049f0213a2222a6e003670))
* MR cleanup ([ad00b06](https://gitlab.com/lmco/hoppr/hoppr/commit/ad00b062c39cc2c0e73dd9c6efae99e6d594281b))
* mypy typing issue, reused variable name kept old type ([96134a2](https://gitlab.com/lmco/hoppr/hoppr/commit/96134a287e8aff528f7851159ce4ce16bbab4f56))
* update collector.py to use new manifest_file_content repositories structure ([23f557d](https://gitlab.com/lmco/hoppr/hoppr/commit/23f557d5b32ebb22bc18e01f1ed05bf2e3de4e76))
* update components to current BOM version ([3f1299f](https://gitlab.com/lmco/hoppr/hoppr/commit/3f1299f572a908ffb47edc757da81bf69f8a258b))

### [0.13.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.1...v0.13.2) (2022-05-16)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.3.0 ([1084a72](https://gitlab.com/lmco/hoppr/hoppr/commit/1084a72aba822d5a731f9d6431c041d66736fde7))

### [0.13.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.0...v0.13.1) (2022-05-12)


### Bug Fixes

* merge from main ([3bea9c1](https://gitlab.com/lmco/hoppr/hoppr/commit/3bea9c1599ef2622782a197eef76a4ac5be430d9))

## [0.13.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.12.2...v0.13.0) (2022-05-11)


### Features

* add docker collector ([470e002](https://gitlab.com/lmco/hoppr/hoppr/commit/470e00299f070ff13f646e838ddc4c72eb152b84))

### [0.12.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.12.1...v0.12.2) (2022-05-11)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.2.1 ([2316121](https://gitlab.com/lmco/hoppr/hoppr/commit/23161215794d83423679290151be4e32e6068776))

### [0.12.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.12.0...v0.12.1) (2022-05-11)


### Bug Fixes

* **deps:** update dependency semantic-release-slack-bot to v3.5.3 ([2879571](https://gitlab.com/lmco/hoppr/hoppr/commit/2879571609b5d585c4fb2462ffe2cdf8cda2e706))

## [0.12.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.11.1...v0.12.0) (2022-05-09)


### Features

* add tar bundler plug-in ([dc0025f](https://gitlab.com/lmco/hoppr/hoppr/commit/dc0025fe0ffd441a52881e1a8f3c728a121c2e20))


### Bug Fixes

* add timetag to bundle file name if it already exists ([97ea4ff](https://gitlab.com/lmco/hoppr/hoppr/commit/97ea4ffaa0e6c911085ca689debe1566e1bdfa79))
* merged from main ([48a597a](https://gitlab.com/lmco/hoppr/hoppr/commit/48a597a3959e6dd703187dedb993a27c14c7c1e1))
* remove 'tryit' app from hopctl ([21a01e9](https://gitlab.com/lmco/hoppr/hoppr/commit/21a01e95f207b9e2094aef072f89d228b2e1a5b1))
* remove base_test_plugin and all dependencies ([3dc715a](https://gitlab.com/lmco/hoppr/hoppr/commit/3dc715a7f0599f221a49a04fa836696d77fa062e))
* update syntax for cobertura report for updated gitlab ([d335aef](https://gitlab.com/lmco/hoppr/hoppr/commit/d335aefb011ea6837f3a4a524ec7bbf377ea7791))

### [0.11.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.11.0...v0.11.1) (2022-05-03)


### Bug Fixes

* add sboms to manifests. ([63bd76a](https://gitlab.com/lmco/hoppr/hoppr/commit/63bd76af34e30e65f0d81bc7ddb4964f7d496e0e))
* added Bom versions 1.3 and 1.4 to manifest.py ([2390ed4](https://gitlab.com/lmco/hoppr/hoppr/commit/2390ed421b392b03fce3181b3eabcfe687b5cfc1))
* load_sbom for local and urls; basic auth only ([4c215f7](https://gitlab.com/lmco/hoppr/hoppr/commit/4c215f779ba195dde5790f41681ba98b328c52c4))
* pre-commit errors ([2eed063](https://gitlab.com/lmco/hoppr/hoppr/commit/2eed063cdf644bd3e083f2d622b1a55625fdab3e))
* recursive child Manifests ([89b19a5](https://gitlab.com/lmco/hoppr/hoppr/commit/89b19a5f5c6202f5fb0221397f54eacdef15fc33))

## [0.11.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.10.3...v0.11.0) (2022-05-03)


### Features

* adding pydantic modesl for cyclone dx ([6972248](https://gitlab.com/lmco/hoppr/hoppr/commit/69722484e7fce78b43fc28956a4ba8b8f6b2d0e4))
* initial processor classes ([2e10f42](https://gitlab.com/lmco/hoppr/hoppr/commit/2e10f4288c2184fcb6d4ebba1b8a730e20a46932))


### Bug Fixes

* Add summary output, comments, result merge method ([95cee10](https://gitlab.com/lmco/hoppr/hoppr/commit/95cee10be3b72c750a6d6860ed003523a46b0ace))
* add unit tests for processor classes ([22610f8](https://gitlab.com/lmco/hoppr/hoppr/commit/22610f8d606481a49dbcd1c9b9d2ba8ea8800c71))
* cleanup ([0481b13](https://gitlab.com/lmco/hoppr/hoppr/commit/0481b13eba15dc3133f1b36926fc4cddf4c28733))
* code review comments, also fixed unit tests to ignore cyclonedx files ([65d5a1a](https://gitlab.com/lmco/hoppr/hoppr/commit/65d5a1a62a06ba12a1ea402678d84d02be57fcaa))
* Correct regex for semantic-release ([d8cae8a](https://gitlab.com/lmco/hoppr/hoppr/commit/d8cae8aaea43225694ced137db33bcd088c26d9f))
* Correct the init py regex ([954b891](https://gitlab.com/lmco/hoppr/hoppr/commit/954b8918e7a08f015b449aa1b2d76a1db93672b2))
* linter issues ([a97d707](https://gitlab.com/lmco/hoppr/hoppr/commit/a97d707ee2effe892ddfc5f3635b0a4ef7c9ebf4))
* linting ([a879623](https://gitlab.com/lmco/hoppr/hoppr/commit/a879623bcafad6b318a2bd546d1d3e832f9ca75b))
* Merge conflicts ([3dbc9a2](https://gitlab.com/lmco/hoppr/hoppr/commit/3dbc9a219aff0533c0b1ad6905844554eaf1948f))
* merge from main ([ab9afe9](https://gitlab.com/lmco/hoppr/hoppr/commit/ab9afe929e453bfb41a383db608c0454e32a961f))
* missed merge conflict, unit test update ([cfcea50](https://gitlab.com/lmco/hoppr/hoppr/commit/cfcea50be2b6f8653e6ca404beab26309ba42fb6))
* renamed sub-stage methods ([f778db5](https://gitlab.com/lmco/hoppr/hoppr/commit/f778db52f28b502723e8653b8ba4c82193b9626b))
* resolve conflict, merge branch 'main' into 'create-hopctl-bundle-command' ([f314995](https://gitlab.com/lmco/hoppr/hoppr/commit/f3149952836f060f18d54854a8aea4494b66d625))
* revert to f-string formatting for log messages, to support future logger wrapping ([4f6335e](https://gitlab.com/lmco/hoppr/hoppr/commit/4f6335e128eabfcbffd70ff0796e5781ec1893d9))
* switch to concurrent.futures for multi-processing ([92e773b](https://gitlab.com/lmco/hoppr/hoppr/commit/92e773b5b3c02070a50c72c9088acb470974f823))
* trailing whitespace ([6c346bd](https://gitlab.com/lmco/hoppr/hoppr/commit/6c346bdf665f1120ab23a964c1e2300b84dd7fb9))
* use pydantic sbom definitions rather than cyclonedx ([bf18e06](https://gitlab.com/lmco/hoppr/hoppr/commit/bf18e066857556901fd191ad3358bb795d04a960))

### [0.10.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.10.2...v0.10.3) (2022-04-28)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.2.0 ([7c487ec](https://gitlab.com/lmco/hoppr/hoppr/commit/7c487ec6ea5d5a9f9b35a12de422322569c82045))

### [0.10.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.10.1...v0.10.2) (2022-04-28)


### Bug Fixes

* **deps:** update dependency click to v8.1.3 ([3d021df](https://gitlab.com/lmco/hoppr/hoppr/commit/3d021dfb1d2d1feec9163abf7f718724cc53b514))

### [0.10.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.10.0...v0.10.1) (2022-04-26)

## [0.10.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.9.2...v0.10.0) (2022-04-26)


### Features

* create collect_raw_plugin ([467fa79](https://gitlab.com/lmco/hoppr/hoppr/commit/467fa79bee5e957e52ee36be2a33324690ffa4eb))


### Bug Fixes

* add exception handling to hoppr_plugin decorator ([d272eba](https://gitlab.com/lmco/hoppr/hoppr/commit/d272ebac2cea6b15488f1eb1264fe49693f85606))
* code review comments ([fbdf1f8](https://gitlab.com/lmco/hoppr/hoppr/commit/fbdf1f89101913341e2c954e85d1f7c085739f3a))

### [0.9.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.9.1...v0.9.2) (2022-04-20)


### Bug Fixes

* **deps:** pin dependency semantic-release-conventional-commits to 3.0.0 ([9c64953](https://gitlab.com/lmco/hoppr/hoppr/commit/9c6495392ecf05392bbd72bf2b0fef27cf0c7065))

### [0.9.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.9.0...v0.9.1) (2022-04-20)


### Bug Fixes

* **docs:** added JSON Schema locations ([a8d7902](https://gitlab.com/lmco/hoppr/hoppr/commit/a8d79027b983d41d4bd6873cdf4d8f522c281276))
* **docs:** Documenting definitions and input files ([94d4e62](https://gitlab.com/lmco/hoppr/hoppr/commit/94d4e6265a7b545c889d5478ad3a3d297b2144c7))
* Add documentation to specify MVP purl support ([8d7b822](https://gitlab.com/lmco/hoppr/hoppr/commit/8d7b822128f1332ff6ad564d701154c35aa43884))
* Correct semantic versioning ([a28dfb1](https://gitlab.com/lmco/hoppr/hoppr/commit/a28dfb18277c6aec0ddaca9d19dd3ec4436b59f9))
* Tweak docs ([01d497c](https://gitlab.com/lmco/hoppr/hoppr/commit/01d497c5525516d08ba75e81bb4993f7226076b9))
* Update mkdocs ([a53c5e4](https://gitlab.com/lmco/hoppr/hoppr/commit/a53c5e4937efed771c425e1b0dad6645c5117416))
* Update schema doc ([1b49040](https://gitlab.com/lmco/hoppr/hoppr/commit/1b49040eb6c857bf6b6acc82bb3f88b45e60d1ba))

## [0.9.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.8.0...v0.9.0) (2022-04-20)


### Features

* test_transfer.py unit tests ([0c19aab](https://gitlab.com/lmco/hoppr/hoppr/commit/0c19aababaeed3b9d08594049a87063f667f8103))
* transfer_file_content and transfer classes ([10ff023](https://gitlab.com/lmco/hoppr/hoppr/commit/10ff0231a32b41266eeb6c8f8b3235861c98f0c7))


### Bug Fixes

* transfer_file_content class and description update ([6d30e76](https://gitlab.com/lmco/hoppr/hoppr/commit/6d30e761a0294fe2169c8032746e137e8dfc7f36))

## [0.8.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.4...v0.8.0) (2022-04-18)


### Features

* add initial manifest config. ([40f955d](https://gitlab.com/lmco/hoppr/hoppr/commit/40f955dfe96fd4f6d60df2cb0d2d114b4810c5f9))
* complete first part of manifest config and add command in main. ([efe3c99](https://gitlab.com/lmco/hoppr/hoppr/commit/efe3c9991f71ebbebcdf1e6076f346ad00ad333d))
* updates to manifest config. ([4866441](https://gitlab.com/lmco/hoppr/hoppr/commit/4866441b77e8b51e93eef141262c472d008eff22))


### Bug Fixes

* Add dependencies ([b7e2a00](https://gitlab.com/lmco/hoppr/hoppr/commit/b7e2a001fe5f7f6d8d2b95b44bffef05d433624b))
* Add dependencies ([b2cd88e](https://gitlab.com/lmco/hoppr/hoppr/commit/b2cd88e021d0f14a4a6573ec6bd0e06f680cee1d))
* Add in bom type and demo ([8ae7603](https://gitlab.com/lmco/hoppr/hoppr/commit/8ae7603e9b628ac847071be2d5ba1a1963c83dfc))
* Add py.typed ([bb25e49](https://gitlab.com/lmco/hoppr/hoppr/commit/bb25e49e08b2c3753e0edf8f644970202a9f3d36))
* Add types-PyYAML to dependencies ([5ee67ff](https://gitlab.com/lmco/hoppr/hoppr/commit/5ee67ff1f9a6910f8d04e461512e148ae3095225))
* cleared pre-commit errors/warnings ([6608b5c](https://gitlab.com/lmco/hoppr/hoppr/commit/6608b5cd71212c40010eb1315f6ef0170ad936e2))
* Correct poetry black command ([cdb218e](https://gitlab.com/lmco/hoppr/hoppr/commit/cdb218e480fd2d316c74abb0b4789fd2f2b21629))
* Create manifest type and validate schema ([614e097](https://gitlab.com/lmco/hoppr/hoppr/commit/614e09766074a7106a70cf5e5ce04e44af7bb6df))
* fix poetry.lock merge. ([dae0fe7](https://gitlab.com/lmco/hoppr/hoppr/commit/dae0fe7c651f631da4bab8c05ff2cb77b189e891))
* manifest can now be loaded ([52d5a4e](https://gitlab.com/lmco/hoppr/hoppr/commit/52d5a4e00053dd3bcf72ff6821c18374f823b7d9))
* pipeline errors ([0d699d0](https://gitlab.com/lmco/hoppr/hoppr/commit/0d699d0b4941197dc9c2a820ee0e0c615914293f))
* pre-commit errors (except hoppr/hoppr_types/bom_type.py) ([392dc36](https://gitlab.com/lmco/hoppr/hoppr/commit/392dc36cd58bb4bb9007de733842bbab908f3071))
* Remove utils ([114a16e](https://gitlab.com/lmco/hoppr/hoppr/commit/114a16e15597973fead5b6491b96f560ee439956))
* repaired unit tests ([776c181](https://gitlab.com/lmco/hoppr/hoppr/commit/776c1814213c183906977b7158e6d9b6babcafd9))
* Represent possible looping concept ([7cc3130](https://gitlab.com/lmco/hoppr/hoppr/commit/7cc313024ac6c722b1aefbb012d5de365f7b911f))
* Run black on hoppr ([75e418d](https://gitlab.com/lmco/hoppr/hoppr/commit/75e418dae2836fd9210217ea9c64ace384922097))
* Run poetry black and reformat ([adaebf8](https://gitlab.com/lmco/hoppr/hoppr/commit/adaebf8a38b7c7820c71ef86c151cefa3233e78b))
* Update components ([fc50d55](https://gitlab.com/lmco/hoppr/hoppr/commit/fc50d5524786272d15ce53437fd16203f2562e76))
* Update poetry ([1c5993a](https://gitlab.com/lmco/hoppr/hoppr/commit/1c5993a76982f0c97cc79cd3a136127930c290c2))
* update poetry.lock ([9e00c64](https://gitlab.com/lmco/hoppr/hoppr/commit/9e00c64cdda704356087bff98878585a789c1413))
* updated CredentialRequiredService to match credential schema ([3aba796](https://gitlab.com/lmco/hoppr/hoppr/commit/3aba7969179d8cbbbb41fe13741d57d7b5e533d1))
* Use decorator ([dbdebb0](https://gitlab.com/lmco/hoppr/hoppr/commit/dbdebb0774e6fd6bc6f6020badc635a0de13732d))

### [0.7.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.3...v0.7.4) (2022-04-18)


### Bug Fixes

* Add py.typed file ([d09ce03](https://gitlab.com/lmco/hoppr/hoppr/commit/d09ce036b00fca195cf7ebdf011116994721675a))

### [0.7.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.2...v0.7.3) (2022-04-15)


### Bug Fixes

* Update pyproject.tml from 3.9 to 3.10 ([6d1e712](https://gitlab.com/lmco/hoppr/hoppr/commit/6d1e712b3b6af0d1a0016e5a46f7ac3194026afa))

### [0.7.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.1...v0.7.2) (2022-04-14)


### Bug Fixes

* **deps:** update dependency click to v8.1.2 ([9239be0](https://gitlab.com/lmco/hoppr/hoppr/commit/9239be0c999e1ceeada5cd1b78bf29fa9b3bf818))

### [0.7.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.0...v0.7.1) (2022-04-14)


### Bug Fixes

* Add in __init__.py and correct linter findings ([fd3361d](https://gitlab.com/lmco/hoppr/hoppr/commit/fd3361d324bd5981890d19f7a18ccaf0366c88a2))
* Add json sbom parser ([6999259](https://gitlab.com/lmco/hoppr/hoppr/commit/69992593b03049fdb1586403609b2b9b3e5c0557))
* Add parser ([0d9be9b](https://gitlab.com/lmco/hoppr/hoppr/commit/0d9be9b85d995ca66ccc9dc259ae966d7531ae90))
* Remove pandas and use dateutil ([a8fc215](https://gitlab.com/lmco/hoppr/hoppr/commit/a8fc215aaaab2be8adf79592f7bf3aaa13da0d29))

## [0.7.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.6.2...v0.7.0) (2022-04-14)


### Features

* add base class for collectors ([f66ef26](https://gitlab.com/lmco/hoppr/hoppr/commit/f66ef26ec89fa9e7a5db39504de8cfa2cf810ee3))


### Bug Fixes

* missed uploading renamed files ([18405ac](https://gitlab.com/lmco/hoppr/hoppr/commit/18405acd7f8919990ffe90764998e891c1d45d3a))
* removed debug statement ([e448ecb](https://gitlab.com/lmco/hoppr/hoppr/commit/e448ecb0083ef68e5058d4780173f887d02080ac))
* rename plugin base class modules to remove redundant '_plugin' ([44d612a](https://gitlab.com/lmco/hoppr/hoppr/commit/44d612a45496ed658010aa8648a2f88896c3ffc1))

### [0.6.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.6.1...v0.6.2) (2022-04-14)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.1.2 ([898f0d2](https://gitlab.com/lmco/hoppr/hoppr/commit/898f0d208b503ced05b3f48729f1b895686aab25))

### [0.6.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.6.0...v0.6.1) (2022-04-13)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.1.1 ([2c90a63](https://gitlab.com/lmco/hoppr/hoppr/commit/2c90a6385b95a5a3d80fce095d6c83ecd163e80e))

## [0.6.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.13...v0.6.0) (2022-04-12)


### Features

* add class for tracking state between plug-ins/stages ([6987ac6](https://gitlab.com/lmco/hoppr/hoppr/commit/6987ac6651419e93349f701c6e8adbfb55fbb128))
* merged initialize method into __init__, replaced if with match/case, removed thread-saftey form base class ([c194648](https://gitlab.com/lmco/hoppr/hoppr/commit/c194648e95b008ba12e0268dac90ece127e1b829))
* upgrade base classes to support staged architecture ([4fae3de](https://gitlab.com/lmco/hoppr/hoppr/commit/4fae3de78cbccbe357636280ac0a5def50b35941))


### Bug Fixes

* add retry logic to base hoppr_plugin ([547e6f7](https://gitlab.com/lmco/hoppr/hoppr/commit/547e6f7ade6d6ab138cd7b1fd247c628ff572c49))
* comments on plug-in base classes ([e38e781](https://gitlab.com/lmco/hoppr/hoppr/commit/e38e781cadebfe41fd6f3ba56366d9079d69cb75))
* convert _pre_ and _post_operation to a decorator (hoppr_process) ([415dfd0](https://gitlab.com/lmco/hoppr/hoppr/commit/415dfd0dd14a0fc3cf271ae0f4d7efdb3428aa43))
* finished updating decorators ([29cf43f](https://gitlab.com/lmco/hoppr/hoppr/commit/29cf43fb81977c4e969307c76025e7610d1baed3))
* include state class ([2455473](https://gitlab.com/lmco/hoppr/hoppr/commit/2455473f6c4962744ba211b14c11ae4e0e3a74df))
* moved methods less likely to be overridden to the bottom of the file ([ae1dd41](https://gitlab.com/lmco/hoppr/hoppr/commit/ae1dd41d2418e96f2e410cc9668609f63f9331df))
* pass state into execute/reverse methods in base classes ([c99f70d](https://gitlab.com/lmco/hoppr/hoppr/commit/c99f70d1790839b5c77a1557e2d0ef576cdf3239))
* re-ran black ([790e655](https://gitlab.com/lmco/hoppr/hoppr/commit/790e65536b337c47cc32e78727775a8d6f3a0d47))
* refactor base classes, replace state with context ([8718169](https://gitlab.com/lmco/hoppr/hoppr/commit/8718169fff65ae54aa64cf5b316797bc310e59e2))
* refactor base classes, replace state with context ([ea6aeef](https://gitlab.com/lmco/hoppr/hoppr/commit/ea6aeef9eb521ac7f8bf4aa03c55301fe9774f18))
* Remove unused exception type ([7b79064](https://gitlab.com/lmco/hoppr/hoppr/commit/7b790649028ae72486c98fbbe1bbd5905157a615))
* unit test ([c2a8038](https://gitlab.com/lmco/hoppr/hoppr/commit/c2a8038203892048651399901c2996095e391cce))

### [0.5.13](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.12...v0.5.13) (2022-04-12)


### Bug Fixes

* Add label ([83e2b82](https://gitlab.com/lmco/hoppr/hoppr/commit/83e2b8227364949d94659c4279a49775a39c93e0))

### [0.5.12](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.11...v0.5.12) (2022-04-11)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.1.0 ([0ee37c0](https://gitlab.com/lmco/hoppr/hoppr/commit/0ee37c04fd99a29f3a3d4c9cdcf8e90e84a02cbb))

### [0.5.11](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.10...v0.5.11) (2022-04-09)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9 ([854c7a4](https://gitlab.com/lmco/hoppr/hoppr/commit/854c7a443ea2e22e80af1433584a7b47e9e63f2f))

### [0.5.10](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.9...v0.5.10) (2022-04-05)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v8.1.0 ([c664047](https://gitlab.com/lmco/hoppr/hoppr/commit/c664047dd9235d8392fc3d1ddef279999c9dd77b))

### [0.5.9](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.8...v0.5.9) (2022-03-31)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v8.0.2 ([8d088e0](https://gitlab.com/lmco/hoppr/hoppr/commit/8d088e07682b238d184afd57751c896d09babcde))

### [0.5.8](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.7...v0.5.8) (2022-03-30)


### Bug Fixes

* versions not reflected in __init__.py or pyporject.toml ([142cd0d](https://gitlab.com/lmco/hoppr/hoppr/commit/142cd0dffe4e76ab0e105a0c4da152f0d39bda3d))

### [0.5.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.6...v0.5.7) (2022-03-29)


### Bug Fixes

* pre-commit updated to use local poetry ([ea76c18](https://gitlab.com/lmco/hoppr/hoppr/commit/ea76c18f20a9aa1c1dced62398663f5a2ebcee58))

### [0.5.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.5...v0.5.6) (2022-03-29)


### Bug Fixes

* **dep:** forced click to downgrade to resolve dependency issue with typer ([a26a8ec](https://gitlab.com/lmco/hoppr/hoppr/commit/a26a8ec91f77a793822278ce03476800f68c2682))
* **docs:** Adding details about semantic versioning and where pypi packages are generated ([02f6028](https://gitlab.com/lmco/hoppr/hoppr/commit/02f6028a7021dd948f2b6b5946e69da781e62616))

### [0.5.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.4...v0.5.5) (2022-03-24)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v8.0.1 ([3580dd2](https://gitlab.com/lmco/hoppr/hoppr/commit/3580dd20b2403d9a989d4e0d6dd3074a80f2efd0))
* add initial schemas (until we have a permanent place for them) ([cf193eb](https://gitlab.com/lmco/hoppr/hoppr/commit/cf193eb05cd3b0ad513961ef504881949f516504))
* add utilities for reading json/yml config files ([3dded41](https://gitlab.com/lmco/hoppr/hoppr/commit/3dded41a438c74f54e45704d1f8342f14fd743ab))

### [0.5.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.3...v0.5.4) (2022-03-24)


### Bug Fixes

* **deps:** update dependency @semantic-release-plus/docker to v3.1.2 ([19ca2a9](https://gitlab.com/lmco/hoppr/hoppr/commit/19ca2a9f085dab7fb1bb1c5f51c492a5f869053d))

### [0.5.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.2...v0.5.3) (2022-03-22)


### Bug Fixes

* add extends to publish-whl ([2f50905](https://gitlab.com/lmco/hoppr/hoppr/commit/2f50905116be925c5a66153cb5fbe15086b99c82))
* get correct RELEASE_VERSION ([0a3dc2d](https://gitlab.com/lmco/hoppr/hoppr/commit/0a3dc2d7d473a6e17643153fc68cdc25a6f102e4))
* remove changes for testing ([34f729f](https://gitlab.com/lmco/hoppr/hoppr/commit/34f729f4b714accb9e9132ac4d58721ce688c913))

### [0.5.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.1...v0.5.2) (2022-03-22)


### Bug Fixes

* **dev:** updates to MR comments to be understandable sentences ([ceedf1f](https://gitlab.com/lmco/hoppr/hoppr/commit/ceedf1fb7d247aedd14fd4237dd2a46af3123521))

### [0.5.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.0...v0.5.1) (2022-03-22)


### Bug Fixes

* **dev:** Updated Issue and MR templates ([0fd8df8](https://gitlab.com/lmco/hoppr/hoppr/commit/0fd8df82fda0156528b852c3a5a7e0be710f82ce))

## [0.5.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.7...v0.5.0) (2022-03-22)


### Features

* Add loggers to plugins, add base class for all plugins ([2bf2d5f](https://gitlab.com/lmco/hoppr/hoppr/commit/2bf2d5f3d269cca7cf4504b5a4f12f9d65891c92))
* define bundler base class ([4f72003](https://gitlab.com/lmco/hoppr/hoppr/commit/4f720032b3b2939eef77f9a671d34a1f510016bc))
* Rename hoppr directory to hopctl ([42bb930](https://gitlab.com/lmco/hoppr/hoppr/commit/42bb93084adbc19ae03177bdf24db04cbd14a113))


### Bug Fixes

* Add methods to plugin_utils to check for required system commands ([0e869ee](https://gitlab.com/lmco/hoppr/hoppr/commit/0e869ee8f6056fbe3e116da08f2a92f80b548135))
* Added factory method to Result object to build from requests.Result object ([fc54269](https://gitlab.com/lmco/hoppr/hoppr/commit/fc5426907feab64d8e23bd9f247b0df6eb7d234b))
* added rules to publish-whl job to match semantic-release ([ff9000d](https://gitlab.com/lmco/hoppr/hoppr/commit/ff9000dac1ff89ad1eee94754a9f23f8cc735bf8))
* Install mypy types in pipeline ([136fc16](https://gitlab.com/lmco/hoppr/hoppr/commit/136fc16f17b50db39834c6b54523de069e39b1b9))
* Merge branch 'main' into plugin-base-class-refinement ([4728edb](https://gitlab.com/lmco/hoppr/hoppr/commit/4728edbed59c1e5a9f9cd946d495b4a4f02ad618))
* need separate stage for publish-whl job ([7c048d9](https://gitlab.com/lmco/hoppr/hoppr/commit/7c048d9199884c52c3c2b324b424b2b13844b842))
* poetry.lock file error ([056d255](https://gitlab.com/lmco/hoppr/hoppr/commit/056d25550cb6c085e18f01c784281901660f466c))
* poetry.lock syntax ([2dfc865](https://gitlab.com/lmco/hoppr/hoppr/commit/2dfc8656f86d41b3f55551291e2e09622af97ff8))
* publish whl file to gitlab registry ([dd17a90](https://gitlab.com/lmco/hoppr/hoppr/commit/dd17a9059ab8105ce80affeff5c519ed6c0c1f2b))
* reverted directory structure to hoppr/hoppr/hoppr ([39e9678](https://gitlab.com/lmco/hoppr/hoppr/commit/39e9678f0aa1e9223aab4c5ad830dc815515a32d))
* specify file for mypy --install-types ([d65a9ca](https://gitlab.com/lmco/hoppr/hoppr/commit/d65a9ca9e90fbb9e4677478ab2c2b3f449553892))
* **test:** made a unit test error message more enterprise-y ([2b128fe](https://gitlab.com/lmco/hoppr/hoppr/commit/2b128fee6090adcc757c82ae735a0adfd0d0ee92))
* **test:** replaced unused plugin name with "plugin_stub_name" ([7d8826d](https://gitlab.com/lmco/hoppr/hoppr/commit/7d8826d6198c2a6cef25cad3e80d2fc753bf397f))
* **test:** replaced unused test plugin name with plugin_stub_name ([1291408](https://gitlab.com/lmco/hoppr/hoppr/commit/1291408210561da0911b7070f2bcf873c4644136))
* **utils:** Split out plugin_utils to a separate module ([9e97dd1](https://gitlab.com/lmco/hoppr/hoppr/commit/9e97dd1ed7a214abbcb0ea76f6dea6fc2234fc2e))
* update pyproject.tml to correctly handle hopctl directory ([82871d3](https://gitlab.com/lmco/hoppr/hoppr/commit/82871d3189ffc600547288bc62e90c1f8ca66b74))

### [0.4.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.6...v0.4.7) (2022-03-21)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v8 ([152ba68](https://gitlab.com/lmco/hoppr/hoppr/commit/152ba68808494a1facee207b080c950b8babc62a))

### [0.4.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.5...v0.4.6) (2022-03-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v7.2.1 ([7516cd4](https://gitlab.com/lmco/hoppr/hoppr/commit/7516cd4e0428a058347b0323b27b049d43a0855f))

### [0.4.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.4...v0.4.5) (2022-03-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v7.2.0 ([01946c5](https://gitlab.com/lmco/hoppr/hoppr/commit/01946c5811819d978ee3de09453aceb1be016e90))

### [0.4.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.3...v0.4.4) (2022-03-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v7.1.1 ([9e3bce5](https://gitlab.com/lmco/hoppr/hoppr/commit/9e3bce5f2e684627f1e84ce0b7c8373fe1c83db2))

### [0.4.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.2...v0.4.3) (2022-03-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v7.1.0 ([2c53c00](https://gitlab.com/lmco/hoppr/hoppr/commit/2c53c007c120b99f661e87ef61b4a7f77b5592bd))

### [0.4.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.1...v0.4.2) (2022-03-17)


### Bug Fixes

* **dev:** added link to pre commit config file ([d611e26](https://gitlab.com/lmco/hoppr/hoppr/commit/d611e26dfe34245263654e7c38d6980a88c2ba99))
* **dev:** moved pre-commit to be listed under dev-dependencies ([8db57ee](https://gitlab.com/lmco/hoppr/hoppr/commit/8db57eec6bbdf9e6f9277950119ed179342c2690))

### [0.4.1](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/lmco/hoppr/hoppr/compare/v0.4.0...v0.4.1) (2022-03-15)


### Bug Fixes

* Corrected error when abstract methods not implemented ([b6b487d](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/lmco/hoppr/hoppr/commit/b6b487d57f75c68677f783a34b955abba5d6ced8))

## [0.4.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.7...v0.4.0) (2022-03-14)


### Features

* Initial 'hello-world' plugin POC ([546ecbe](https://gitlab.com/lmco/hoppr/hoppr/commit/546ecbeec464d66afb401101f44c007224deb7de))


### Bug Fixes

* Add mypy type checker to pipeline ([ce677e2](https://gitlab.com/lmco/hoppr/hoppr/commit/ce677e25bd686e83f9fb67f24abbc1741202ae52))
* Correct class naming convention ([237433d](https://gitlab.com/lmco/hoppr/hoppr/commit/237433d31e088685bed26a6df8af601e369b4982))
* Make CollectorPluginBase.get_version an abstractmethod ([18daf8a](https://gitlab.com/lmco/hoppr/hoppr/commit/18daf8a35972088f40c7168bca89da3ab53b97fb))
* Merge branch 'main' into initial-plugin-structure ([c9ba5d6](https://gitlab.com/lmco/hoppr/hoppr/commit/c9ba5d62051eceb1a70741c043000e7f15ed1b0e))
* move coverage to dev-dependencies ([14f6667](https://gitlab.com/lmco/hoppr/hoppr/commit/14f6667e757ae7052eb5fc8df00ebde9af953e65))
* Move publish to semantic-release ([e8960f6](https://gitlab.com/lmco/hoppr/hoppr/commit/e8960f6280f15c2ad128758f78f61110b25bfd09))
* Move pylint, pytest, coverage, and pylint to dev-dependencies ([b77b2da](https://gitlab.com/lmco/hoppr/hoppr/commit/b77b2daa0b1c2872474440ded8c928dc031d8378))
* refacored utils for clarity, removed comments ([5a05098](https://gitlab.com/lmco/hoppr/hoppr/commit/5a050983f3fb80d704b43e09e71ea30d18379167))
* Remove check for RELEASE_VERSION environment variable from version command ([6c18027](https://gitlab.com/lmco/hoppr/hoppr/commit/6c180270b2532a533b7652920495e9edbc4a2781))
* removed schemas.py ([0598272](https://gitlab.com/lmco/hoppr/hoppr/commit/0598272811cbbec9aa0938d6d9445c49a53af79d))
* update collector.py to fix mypy errors ([26935ac](https://gitlab.com/lmco/hoppr/hoppr/commit/26935acf5bae5bc8e2e829ac1798e443639e5ea2))
* update collectory.py for black ([3bfd10e](https://gitlab.com/lmco/hoppr/hoppr/commit/3bfd10ee066c2f4f1209bbf9833230407ae4e5d6))
* Update formatting with black ([eb29b3f](https://gitlab.com/lmco/hoppr/hoppr/commit/eb29b3f891d0034dc1a18fa30d99f4017d2875b9))
* update unit tests for new class names ([1d6c3ab](https://gitlab.com/lmco/hoppr/hoppr/commit/1d6c3abc7cc0f02f0a606146bebf1a2c05a07336))
* version reporting, add build to pipeline, include tests ([abd32bf](https://gitlab.com/lmco/hoppr/hoppr/commit/abd32bfd1f22f56d672c4a07e7887c30518c043c))

### [0.3.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.6...v0.3.7) (2022-03-09)


### Bug Fixes

* **docs:** adding docs for gitpod to development ([46e9479](https://gitlab.com/lmco/hoppr/hoppr/commit/46e9479c70c99289e656d06a58495a3dd4ce8c21))
* **docs:** adding review app environments for docs on branches ([140fc8b](https://gitlab.com/lmco/hoppr/hoppr/commit/140fc8beb7213cac95df73c3715c93358da9d8ec))
* **docs:** Updating contributing docs to reference Conventional Commits ([5bf4539](https://gitlab.com/lmco/hoppr/hoppr/commit/5bf45396f923fd3fbebf0db18c6013ce90926952))

### [0.3.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.5...v0.3.6) (2022-03-08)


### Bug Fixes

* **dev:** adding quick gitpods config. ([380c1b3](https://gitlab.com/lmco/hoppr/hoppr/commit/380c1b3e873f984c9ed14cc4db2056e731cd3d94))
* **dev:** automatically start mkdocs in gitpods ([818e666](https://gitlab.com/lmco/hoppr/hoppr/commit/818e666f8792eb85c49e6a37e5bd639e4698d371))

### [0.3.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.4...v0.3.5) (2022-03-03)


### Bug Fixes

* Extract Renovate into new project ([375b5ab](https://gitlab.com/lmco/hoppr/hoppr/commit/375b5ab8243c789ea6cddaf867071f0c217162ed))

### [0.3.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.3...v0.3.4) (2022-03-01)


### Bug Fixes

* **docs:** adding issue/mr templates, contributing guidelines, and development docs ([18f59ca](https://gitlab.com/lmco/hoppr/hoppr/commit/18f59cae8b851e036f51b4f2a315567ace7cd4a0))

### [0.3.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.2...v0.3.3) (2022-03-01)


### Bug Fixes

* Don't run semantic release and other jobs on schedule ([9e82e21](https://gitlab.com/lmco/hoppr/hoppr/commit/9e82e217bc9e0608bddbba72f444b55df299f30a))
* Hotfix for pages-test ([e6b5d3a](https://gitlab.com/lmco/hoppr/hoppr/commit/e6b5d3a6833a490ab299def667992be6b4ff292b))

### [0.3.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.1...v0.3.2) (2022-02-28)


### Bug Fixes

* **deps:** update semantic-release monorepo ([52127f8](https://gitlab.com/lmco/hoppr/hoppr/commit/52127f8f618d4424e1f04e7b801e077cb2529356))

### [0.3.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.0...v0.3.1) (2022-02-28)


### Bug Fixes

* **deps:** update dependency @semantic-release-plus/docker to v3 ([f413b61](https://gitlab.com/lmco/hoppr/hoppr/commit/f413b6159f669e34b0edd43341a62e3472162037))
* **deps:** update dependency ansi-regex to v6 ([ab2553b](https://gitlab.com/lmco/hoppr/hoppr/commit/ab2553b898ba8387b38a6327265fc27ff0ebc9ee))
* **deps:** update dependency semantic-release-slack-bot to v3 ([ddc4ac3](https://gitlab.com/lmco/hoppr/hoppr/commit/ddc4ac38c8a0985eb824f4a85ed96770a63f226c))

## [0.3.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.2.0...v0.3.0) (2022-02-28)


### Features

* **docs:** adding intro description ([4f5ecb2](https://gitlab.com/lmco/hoppr/hoppr/commit/4f5ecb26d8b911854ba875992c7f0ca1c070b653))


### Bug Fixes

* **deps:** pin dependencies ([bb3a738](https://gitlab.com/lmco/hoppr/hoppr/commit/bb3a738fca593785770e132725585d9912c3d853))
* Add in workflow to handle MR's ([7fef364](https://gitlab.com/lmco/hoppr/hoppr/commit/7fef3642937d5ef8a840bbb1ef8a5719461a484e))
* Update renovate ([6b847df](https://gitlab.com/lmco/hoppr/hoppr/commit/6b847dfdfd46f92bb617cc157f0f1bb4e95b2843))

## [0.2.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.1.1...v0.2.0) (2022-02-28)


### Features

* **docs:** adding mkdocs structure and gitlab pages CI ([399975c](https://gitlab.com/lmco/hoppr/hoppr/commit/399975ca4c154980fe479d447f78b236999b9437))


### Bug Fixes

* Add renovate ([90cc5e3](https://gitlab.com/lmco/hoppr/hoppr/commit/90cc5e3c9f1c1a7c1c7607bced322b92f8e97a03))
* Add semantic release ([cbf6717](https://gitlab.com/lmco/hoppr/hoppr/commit/cbf6717a9957cf281d32bf6610eb26df433cebb8))
* Clean up and add templates ([d360ba5](https://gitlab.com/lmco/hoppr/hoppr/commit/d360ba51f9081f12647229e1348affad6755fe03))
* Correct the echo, use handle ([73199aa](https://gitlab.com/lmco/hoppr/hoppr/commit/73199aaea24b308aa39c7f2822595373a884990d))
* Hotfix for semantic release publish ([2e8353e](https://gitlab.com/lmco/hoppr/hoppr/commit/2e8353eef2e4cf47c9a5ed3bc21917985a097136))
* Hotfix the image entrypoint ([36427b1](https://gitlab.com/lmco/hoppr/hoppr/commit/36427b18a1284a34331246e6f8d685f03cfeed6b))
* Release semantic release and setup renovate schedule ([08e7364](https://gitlab.com/lmco/hoppr/hoppr/commit/08e7364c782e924ced1dbdb8ef438b33db327554))
* Remove errant echo command ([4d98743](https://gitlab.com/lmco/hoppr/hoppr/commit/4d987437c96aa18a93aa2c396513e397e77e019c))
* Stages ([7fdaf0a](https://gitlab.com/lmco/hoppr/hoppr/commit/7fdaf0a31f51c983088e47df45263cd2e3376654))
